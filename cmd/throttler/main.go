package main

import (
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strconv"
	"time"

	"github.com/ulule/limiter/v3"
	"github.com/ulule/limiter/v3/drivers/store/memory"
)

func main() {
	remote, err := url.Parse("http://art-near-me-app:81")
	if err != nil {
		panic(err)
	}

	proxy := httputil.NewSingleHostReverseProxy(remote)

	t := Throttler{
		limiter: limiter.New(
			memory.NewStore(),
			limiter.Rate{
				Period: 1 * time.Hour,
				Limit:  resolveRequestLimit(),
			}),
		callback: handler(proxy),
	}

	err = http.ListenAndServe(":8079", t)
	if err != nil {
		panic(err)
	}
}

type Throttler struct {
	limiter  *limiter.Limiter
	callback func(http.ResponseWriter, *http.Request)
}

func (t Throttler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ip, err := resolveIP(r)

	if err != nil {
		log.Printf("error obtaining ip address: %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	limiterCtx, err := t.limiter.Get(r.Context(), ip)
	if err != nil {
		log.Printf("IPRateLimit - ipRateLimiter.Get - err: %v, %s on %s", err, ip, r.URL)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	h := w.Header()
	h.Set("X-RateLimit-Limit", string(limiterCtx.Limit))
	h.Set("X-RateLimit-Remaining", string(limiterCtx.Remaining))
	h.Set("X-RateLimit-Reset", string(limiterCtx.Reset))

	if limiterCtx.Reached {
		log.SetOutput(os.Stderr)
		log.Printf("Too Many Requests from %s on %s", ip, r.URL)
		w.WriteHeader(429)
		_, err := w.Write([]byte("{\"msg\":\"too many requests\"}"))

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		return
	}
	t.callback(w, r)
}

func handler(p *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.SetOutput(os.Stderr)
		log.Println(r.URL)
		w.Header().Set("X-Ben", "Rad")
		p.ServeHTTP(w, r)
	}
}

func resolveIP(r *http.Request) (string, error) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)

	if err != nil {
		return "", err
	}

	if forIP := r.Header.Get("X-FORWARDED-FOR"); forIP != "" {
		ip = forIP
	}
	return ip, nil
}

func resolveRequestLimit() int64 {
	v := os.Getenv("REQUEST_LIMIT")
	if v == "" {
		return 500
	}
	intV, err := strconv.ParseInt(v, 10, 64)

	if err != nil {
		panic(err)
	}
	return intV
}
