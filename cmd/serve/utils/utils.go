package utils

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func ParseParamAsInt(r *http.Request, paramName string) (int, error) {
	params := mux.Vars(r)
	sid, ok := params[paramName]
	if !ok {
		return 0, fmt.Errorf("unable to find param: %s", paramName)
	}

	id, err := strconv.Atoi(sid)
	if err != nil {
		return 0, fmt.Errorf("unable to convert to int: %s", sid)
	}
	return id, nil
}
