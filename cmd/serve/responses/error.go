package responses

// Error represents a handler error. It provides methods for a HTTP status
// code and embeds the built-in error interface.
type StatusError interface {
	error
	Status() int
}

// StatusError represents an error with an associated HTTP status code.
type statusError struct {
	Code int
	Err  error
}
