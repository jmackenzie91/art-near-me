package responses

import (
	"errors"
	"fmt"
	"net/http"
)

// ResourceNotFound generic json response to unfound resource
func ResourceNotFound(resource string, id int) statusError {
	return statusError{
		Err:  fmt.Errorf("{\"msg\":\"Resource Not Found '%s' with id %d\"", resource, id),
		Code: http.StatusNotFound,
	}
}

// InternalServerError generic json response to 500 error
func InternalServerError(err error) statusError {
	return statusError{
		Err:  err,
		Code: http.StatusInternalServerError,
	}
}

// InternalServerError generic json response to 500 error
var BadRequest = statusError{
	Err:  errors.New("{\"msg\":\"Bad Request\"}"),
	Code: http.StatusBadRequest,
}
