package handlers

import (
	"errors"
	"fmt"
	"net/http"
)

// Error represents a handler error. It provides methods for a HTTP status
// code and embeds the built-in error interface.
type StatusError interface {
	error
	Status() int
}

// StatusError represents an error with an associated HTTP status code.
type statusError struct {
	Code int
	Err  error
}

// Allows StatusError to satisfy the error interface.
func (se statusError) Error() string {
	return se.Err.Error()
}

// Returns our HTTP status code.
func (se statusError) Status() int {
	return se.Code
}

// ResourceNotFound generic json response to unfound resource
func ResourceNotFound(resource string, id int) statusError {
	return statusError{
		Err:  fmt.Errorf("{\"msg\":\"Resource Not Found '%s' with id %d\"", resource, id),
		Code: http.StatusNotFound,
	}
}

// InternalServerError generic json response to 500 error
func InternalServerError(err error) statusError {
	return statusError{
		Err:  err,
		Code: http.StatusInternalServerError,
	}
}

// InternalServerError generic json response to 500 error
var BadRequest = statusError{
	Err:  errors.New("{\"msg\":\"Bad Request\"}"),
	Code: http.StatusBadRequest,
}
