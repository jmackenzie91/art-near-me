package handlers

import (
	"html/template"
	"net/http"
	"os"
)

// Index is the make do index for the / endpoint, atm it just links to a plaing html page
func Index(w http.ResponseWriter, r *http.Request) error {

	tmpl, err := template.ParseFiles(string(os.Getenv("HTML_ROOT") + "/index.html"))

	if err != nil {
		return err
	}

	err = tmpl.Execute(w, nil)

	if err != nil {
		return err
	}
	return nil
}
