package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/repositories"

	"github.com/johnmackenzie91/go-pagination/paginator"
	"github.com/sirupsen/logrus"
)

type ArtistsIndex struct {
	Repo   repositories.IArtists
	Logger *logrus.Logger
}

func (a ArtistsIndex) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var artists []domain.Artist
	var totalArtists int
	var err error

	// count total artists in db
	if totalArtists, err = a.Repo.Count(); err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	pb := paginator.New(totalArtists, paginator.DefaultLimit(100))
	p, err := pb.StateFromRequest(r)

	if err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if artists, err = a.Repo.FindAll(p.GetOffset(), p.Meta.ItemsPerPage); err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	p.Data = artists

	out, err := json.Marshal(p)

	if err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if _, err = w.Write(out); err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

type ArtistsGet struct {
	Repo   repositories.IArtists
	Logger *logrus.Logger
}

func (a ArtistsGet) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	artist := FromRequest(r)
	if artist == nil {
		a.Logger.Info(fmt.Sprintf("artist not found in context"))
		w.WriteHeader(http.StatusNotFound)
		return
	}
	byt, err := json.Marshal(artist)
	if err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	_, err = w.Write(byt)

	if err != nil {
		a.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

// move me
func FromRequest(r *http.Request) *domain.Artist {
	v, ok := r.Context().Value("artist").(*domain.Artist)

	if !ok {
		return nil
	}
	return v
}

// ArtistIdWithPaintings is the endpoint handler for GET /artists/{id}/paintings
type ArtistIdWithPaintings struct {
	ArtistRepo   repositories.IArtists
	PaintingRepo repositories.IPaintings
}

// Run the actual handler for ArtistIdWithPaintings
func (a ArtistIdWithPaintings) Run(w http.ResponseWriter, r *http.Request) error {
	var artist *domain.Artist
	var totalPaintingsForArtist int
	var err error

	id, err := ParseParamAsInt(r, "id")
	if err != nil {
		return ResourceNotFound("artists", 0)
	}

	if artist, err = a.ArtistRepo.FindByID(id); err != nil {
		return InternalServerError(err)
	}

	if artist == nil || !artist.HasID() {
		return ResourceNotFound("artist", id)
	}

	if totalPaintingsForArtist, err = a.PaintingRepo.Count(artist); err != nil {
		return InternalServerError(err)
	}

	// pagination
	p := paginator.New(totalPaintingsForArtist, paginator.DefaultLimit(100))
	pagin, err := p.StateFromRequest(r)

	if err != nil {
		return InternalServerError(err)
	}

	if artist, err = a.ArtistRepo.FindByIDWithPaintings(id, pagin.GetOffset(), pagin.GetLimit()); err != nil {
		return InternalServerError(err)
	}

	pagin.Data = artist.Paintings
	out, err := json.Marshal(pagin)

	if err != nil {
		return InternalServerError(err)
	}

	if _, err = w.Write(out); err != nil {
		return InternalServerError(err)
	}
	return nil
}
