package handlers

import (
	"net/url"
	"no_vcs/me/art-near-me/pkg/domain"
	"strconv"
)

func GetQueryParamInt(qString *url.URL, name string, def, min, max int) int {
	v := qString.Query().Get(name)
	if v == "" {
		return def
	}
	i, err := strconv.ParseInt(v, 10, 61)
	if err != nil {
		return def
	}
	if int(i) >= min && int(i) <= max {
		return int(i)
	}
	return def
}

func GetQueryParamCoord(qString *url.URL, name string, def *domain.Coords) *domain.Coords {
	v := qString.Query().Get(name)
	if v != "" {

		loc, err := domain.CoordsFromString(v)
		if err != nil {
			return def
		}
		return loc
	}
	return def
}
