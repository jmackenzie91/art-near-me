package handlers

import (
	"encoding/json"
	"net/http"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/repositories"

	"github.com/sirupsen/logrus"
)

// PaintingsByDistance is the endpoint handler for GET /artists
type PaintingsByDistance struct {
	PaintingRepo repositories.IPaintings
	Logger       *logrus.Logger
}

// Run the actual handler for ArtistsIndex
func (p PaintingsByDistance) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	limit := GetQueryParamInt(r.URL, "limit", 100, 0, 1000)
	distance := GetQueryParamInt(r.URL, "distance", 100000, 100000, 10000000)
	leeds := &domain.Coords{Longitude: -1.54785, Latitude: 53.79648}
	location := GetQueryParamCoord(r.URL, "location", leeds)

	res, err := p.PaintingRepo.FindByLocation(location, distance, limit)

	if err != nil {
		p.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	out, err := json.Marshal(res)

	if err != nil {
		p.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if _, err = w.Write(out); err != nil {
		p.Logger.Error(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
