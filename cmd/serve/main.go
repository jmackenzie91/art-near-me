package main

import (
	"fmt"
	"net/http"
	"no_vcs/me/art-near-me/cmd/serve/api"
	"no_vcs/me/art-near-me/cmd/serve/app"
	"no_vcs/me/art-near-me/cmd/serve/logger"
	"no_vcs/me/art-near-me/cmd/serve/middleware"
	"no_vcs/me/art-near-me/internal"
	"no_vcs/me/art-near-me/pkg/crud"
	"no_vcs/me/art-near-me/pkg/repositories"
	"os"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		//// setup resolver (that resolves services ie logger, db etc
		c := internal.NewConfiguration()
		res := internal.NewResolver(c)

		//// init data access layer
		d := crud.NewDAL(res.ResolveDB(), res.ResolveLogger())

		l := logrus.New()
		l.SetFormatter(&logrus.JSONFormatter{})
		errLogger := logger.New(l)

		// dependencies
		artistRepo := repositories.NewArtists(d)
		paintingRepo := repositories.NewPaintings(d)

		// middleware
		reqMetrics := api.Metrics()
		hasArtist := middleware.HasArtist(artistRepo)

		routes := api.NewAPI("art-near-me", mux.NewRouter(), errLogger)

		routes.Handle(
			"/v1/healthcheck",
			"healthcheck",
			reqMetrics("healthcheck", app.Healthcheck())).
			Methods("GET")

		routes.Handle(
			"/artists",
			"artists_index",
			reqMetrics("artists_index", app.ArtistsIndex(artistRepo)),
		).Methods("GET")

		routes.Handle(
			"/artists/{id}",
			"artists_id",
			reqMetrics("artists_id", hasArtist(app.ArtistsID())),
		).Methods("GET")

		routes.Handle(
			"/artists/{id}/paintings",
			"artists_paintings",
			reqMetrics("artists_paintings", hasArtist(app.ArtistPaintings(artistRepo, paintingRepo))),
		).Methods("GET")

		routes.Handle(
			"/paintings",
			"paintings",
			reqMetrics("paintings", app.Paintings(paintingRepo)),
		).Methods("GET")

		if err := http.ListenAndServe(":81", routes); err != nil {
			panic(err)
		}
	},
}

func main() {
	if err := serveCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
