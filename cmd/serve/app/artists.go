package app

import (
	"encoding/json"
	"net/http"
	"no_vcs/me/art-near-me/cmd/serve/api"
	"no_vcs/me/art-near-me/cmd/serve/utils"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/repositories"

	"github.com/johnmackenzie91/go-pagination/paginator"
)

func ArtistsIndex(repo repositories.IArtists) api.Handler {
	return func(r *http.Request) api.Response {
		var artists []domain.Artist
		var totalArtists int
		var err error

		// count total artists in db
		if totalArtists, err = repo.Count(); err != nil {
			return api.NewResponse(err)
		}
		pb := paginator.New(totalArtists, paginator.DefaultLimit(100))
		p, err := pb.StateFromRequest(r)

		if err != nil {
			return api.NewResponse(err)
		}

		if artists, err = repo.FindAll(p.GetOffset(), p.Meta.ItemsPerPage); err != nil {
			return api.NewResponse(err)
		}

		p.Data = artists

		out, err := json.Marshal(p)

		if err != nil {
			return api.NewResponse(err)
		}

		return api.NewResponse(out)
	}
}

func ArtistsID() api.Handler {
	return func(r *http.Request) api.Response {
		artist := FromRequest(r)
		if artist == nil {
			return api.NewResponse(http.StatusNotFound)
		}
		byt, err := json.Marshal(artist)
		if err != nil {
			return api.NewResponse(err)
		}
		return api.NewResponse(byt)
	}
}

func ArtistPaintings(artistRepo repositories.IArtists, paintingRepo repositories.IPaintings) api.Handler {
	return func(r *http.Request) api.Response {
		var artist *domain.Artist
		var totalPaintingsForArtist int
		var err error

		id, err := utils.ParseParamAsInt(r, "id")
		if err != nil {
			return api.NewResponse(http.StatusNotFound)
		}

		if artist, err = artistRepo.FindByID(id); err != nil {
			return api.NewResponse(err)
		}

		if artist == nil || !artist.HasID() {
			return api.NewResponse(http.StatusNotFound)
		}

		if totalPaintingsForArtist, err = paintingRepo.Count(artist); err != nil {
			return api.NewResponse(err)
		}

		// pagination
		p := paginator.New(totalPaintingsForArtist, paginator.DefaultLimit(100))
		pagin, err := p.StateFromRequest(r)

		if err != nil {
			return api.NewResponse(err)
		}

		if artist, err = artistRepo.FindByIDWithPaintings(id, pagin.GetOffset(), pagin.GetLimit()); err != nil {
			return api.NewResponse(err)
		}

		pagin.Data = artist.Paintings
		out, err := json.Marshal(pagin)

		if err != nil {
			return api.NewResponse(err)
		}

		return api.NewResponse(out)
	}
}

// move me
func FromRequest(r *http.Request) *domain.Artist {
	v, ok := r.Context().Value("artist").(*domain.Artist)

	if !ok {
		return nil
	}
	return v
}
