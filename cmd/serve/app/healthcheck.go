package app

import (
	"net/http"
	"no_vcs/me/art-near-me/cmd/serve/api"
)

func Healthcheck() api.Handler {
	return func(r *http.Request) api.Response {
		return api.NewResponse([]byte("OK"))
	}
}
