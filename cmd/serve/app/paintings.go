package app

import (
	"encoding/json"
	"net/http"
	"no_vcs/me/art-near-me/cmd/serve/api"
	"no_vcs/me/art-near-me/cmd/serve/handlers"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/repositories"
)

func Paintings(paintingsRepo repositories.IPaintings) api.Handler {
	return func(r *http.Request) api.Response {
		limit := handlers.GetQueryParamInt(r.URL, "limit", 100, 0, 1000)
		distance := handlers.GetQueryParamInt(r.URL, "distance", 100000, 100000, 10000000)
		leeds := &domain.Coords{Longitude: -1.54785, Latitude: 53.79648}
		location := handlers.GetQueryParamCoord(r.URL, "location", leeds)

		res, err := paintingsRepo.FindByLocation(location, distance, limit)

		if err != nil {
			return api.NewResponse(err)
		}

		out, err := json.Marshal(res)

		if err != nil {
			return api.NewResponse(err)
		}
		return api.NewResponse(out)
	}
}
