package middleware

import (
	"context"
	"net/http"
	"no_vcs/me/art-near-me/cmd/serve/api"
	"no_vcs/me/art-near-me/cmd/serve/utils"
	"no_vcs/me/art-near-me/pkg/repositories"
)

func HasArtist(repo repositories.IArtists) api.MiddlewareFunc {
	return func(handler api.Handler) api.Handler {
		return func(r *http.Request) api.Response {
			// Our middleware logic goes here...
			id, err := utils.ParseParamAsInt(r, "id")
			if err != nil {
				return api.NewResponse(err)
			}

			artist, err := repo.FindByID(id)

			if err != nil {
				return api.NewResponse(err)
			}

			if artist == nil {
				return api.NewResponse(http.StatusNotFound)
			}

			ctx := context.WithValue(r.Context(), "artist", artist)
			return handler(r.WithContext(ctx))
		}
	}
}
