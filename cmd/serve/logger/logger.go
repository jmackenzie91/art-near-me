package logger

import "github.com/sirupsen/logrus"

type CustomLogger interface {
	Error(err error)
	Log(interface{})
}

type customLogger struct {
	logger *logrus.Logger
}

func New(log *logrus.Logger) CustomLogger {
	return customLogger{
		logger: log,
	}
}

type Context interface {
	Context() logrus.Fields
}

func (c customLogger) Error(err error) {
	ctx, ok := err.(Context)

	if !ok {

		if err != nil {
			c.logger.Error(err)
		}
		return
	}
	c.logger.WithFields(ctx.Context()).Error(err)
}

func (c customLogger) Log(i interface{}) {
	switch i.(type) {
	case string:
		c.logger.Info(i)
	}
}
