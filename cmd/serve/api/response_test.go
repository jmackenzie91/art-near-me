package api_test

import (
	"errors"
	"net/http"
	"testing"

	"no_vcs/me/art-near-me/cmd/serve/api"

	"github.com/stretchr/testify/assert"
)

func TestNewResponse(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name             string
		params           []interface{}
		expectedResponse api.Response
	}{
		{
			name: "200 returned by default, no body",
			expectedResponse: api.Response{
				StatusCode: http.StatusOK,
			},
		},
		{
			name: "201 returned, with body",
			params: []interface{}{
				http.StatusCreated,
				[]byte("hello world!"),
			},
			expectedResponse: api.Response{
				StatusCode: http.StatusCreated,
				Body:       []byte("hello world!"),
			},
		},
		{
			name: "500 return when error supplied, no status set",
			params: []interface{}{
				errors.New("some error"),
			},
			expectedResponse: api.Response{
				StatusCode: http.StatusInternalServerError,
				Err:        errors.New("some error"),
			},
		},
		{
			name: "401 returned with error",
			params: []interface{}{
				http.StatusUnauthorized, errors.New("some error"),
			},
			expectedResponse: api.Response{
				StatusCode: http.StatusUnauthorized,
				Err:        errors.New("some error"),
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// arrange
			t.Parallel()
			// act
			res := api.NewResponse(tc.params...)
			// assert
			assert.Equal(t, tc.expectedResponse, res)
		})
	}
}
