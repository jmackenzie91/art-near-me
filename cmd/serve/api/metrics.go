package api

import (
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	statusLabel = "status"
	routeLabel  = "route_name"
	methodLabel = "method"
)

var (
	duration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "art_near_me_http_request_duration_seconds",
			Help:    "Request duration seconds for HTTP Request",
			Buckets: []float64{0.01, 0.025, 0.05, 0.075, 0.1, 0.15, 0.2, 0.25, 0.3, 0.4, 0.5, 1, 2},
		}, []string{methodLabel, routeLabel})

	// counter - Create a counter metric to track executions of key functions
	reqCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "art_near_me_http_request_count",
			Help: "Total number of HTTP requests",
		},
		[]string{statusLabel, routeLabel})

	errCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "art_near_me_server_error_count",
			Help: "Total number of 5xx errors",
		},
		[]string{statusLabel, routeLabel},
	)
)

func init() {
	prometheus.MustRegister(duration)
	prometheus.MustRegister(reqCounter)
	prometheus.MustRegister(errCounter)
}

// Metrics this is function is purely for aesthetics
// it allows us to assign the inner metrics() func to a variable and use that as middleware
// in the routes as reqMetrics("label", handlerFun)
func Metrics() func(label string, h Handler) Handler {
	return metrics
}

func metrics(label string, h Handler) Handler {
	return func(r *http.Request) Response {
		reqStart := time.Now()

		res := h(r)

		statusCodeStr := strconv.Itoa(res.StatusCode)

		if res.Err != nil {
			errCounter.With(prometheus.Labels{
				statusLabel: statusCodeStr,
				routeLabel:  label,
			})
		}

		reqCounter.With(prometheus.Labels{
			statusLabel: statusCodeStr,
			routeLabel:  label,
		}).Inc()

		reqFin := time.Now()

		duration.With(prometheus.Labels{
			methodLabel: r.Method,
			routeLabel:  label,
		}).Observe(reqFin.Sub(reqStart).Seconds())

		return res
	}
}
