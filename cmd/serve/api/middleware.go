package api

type MiddlewareFunc func(handler Handler) Handler
