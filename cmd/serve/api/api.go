package api

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type API interface {
	http.Handler
	Handle(path, name string, handler Handler) *mux.Route
	Handle404(handler Handler)
}
type api struct {
	router *mux.Router
	logger CommonLogger
}

type CommonLogger interface {
	Error(err error)
}

func NewAPI(name string, r *mux.Router, l CommonLogger) API {
	r.Handle("/v1/metrics", promhttp.Handler())
	return &api{
		router: r,
		logger: l,
	}
}

func (a api) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	a.router.ServeHTTP(w, r)
}

func (a *api) Handle404(handler Handler) {
	a.router.NotFoundHandler = notFoundWrapper{
		Handler: handler,
		logger:  a.logger,
	}
}
