package api

import (
	"fmt"
	"net/http"
)

type Response struct {
	StatusCode int
	Body       []byte
	Err        error
}

func NewResponse(args ...interface{}) Response {
	r := Response{}

	for _, arg := range args {
		switch arg := arg.(type) {
		case int:
			r.StatusCode = arg
		case []byte:
			r.Body = arg
		case error:
			r.Err = arg
		default:
			panic(fmt.Sprintf("bad call to NewResponse: type: %+v", arg))
		}
	}

	if r.StatusCode == 0 {
		if r.Err == nil {
			r.StatusCode = http.StatusOK
		} else {
			r.StatusCode = http.StatusInternalServerError
		}
	}

	return r
}
