package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Handler func(r *http.Request) Response

func (a api) Handle(path, name string, handler Handler) *mux.Route {
	return a.router.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {

		res := handler(r)

		if res.Err != nil {
			a.logger.Error(res.Err)
		}
		w.WriteHeader(res.StatusCode)

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Cache-Control: ", "No-Cache")

		if _, err := w.Write(res.Body); err != nil {
			a.logger.Error(err)
		}
	}).Name(name)
}
