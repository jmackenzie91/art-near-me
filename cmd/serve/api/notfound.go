package api

import (
	"net/http"
)

type notFoundWrapper struct {
	Handler Handler
	logger  CommonLogger
}

func (n notFoundWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	res := n.Handler(r)

	if res.Err != nil {
		n.logger.Error(res.Err)
	}
	w.WriteHeader(res.StatusCode)

	if _, err := w.Write(res.Body); err != nil {
		n.logger.Error(err)
	}
}
