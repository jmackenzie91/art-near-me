package main

import (
	"fmt"
	"io"
	"no_vcs/me/art-near-me/cmd/index/strategies"
	"no_vcs/me/art-near-me/pkg/domain"
	"os"
	"sync"

	"github.com/spf13/cobra"
	"gopkg.in/birkirb/loggers.v1"
)

type index struct {
	logger        loggers.Contextual
	artistService ArtistLoggingService
	todo          []strategies.Index
}

// Run runs the command
func (i index) Run(cmd *cobra.Command, args []string) {

	fmt.Println("Index Started")

	artistStore := ArtistStore{}
	var err error
	var source io.ReadCloser
	var loc *domain.Location

	for _, index := range i.todo {
		artistStore.Artist = i.artistService.FindArtist(index.GetName())

		if artistStore.Artist == nil {
			artistStore.Artist = &domain.Artist{
				Name: index.GetName(),
			}
			i.artistService.AddArtist(artistStore.Artist)
			i.logger.Infof("Added New Artist %s", artistStore.Artist.Name)
		}

		if err := i.artistService.GetError(); err != nil {
			os.Exit(1)
		}

		if source, err = index.GetSource(); err != nil {
			i.logger.Error(err)
			break
		}

		paintingsChan, errorChan := index.Strategy(source)

		wg := sync.WaitGroup{}
		wg.Add(1)
		go func() {
			for err := range errorChan {
				fmt.Println(err)
			}
		}()

		go func() {
			for painting := range paintingsChan {

				if artistStore.HasPainting(painting) {
					break
				}

				loc = i.artistService.FindLocation(painting.Location)

				if loc == nil {
					i.artistService.AddLocation(painting.Location)
				} else {
					painting.Location = loc
				}

				i.artistService.AddPainting(painting, artistStore.Artist)

				if err := i.artistService.GetError(); err != nil {
					os.Exit(1)
				}
				i.logger.Infof("added new painting %s, for artist %s", painting.Title, artistStore.Artist.Name)
			}
			wg.Done()
		}()

		wg.Wait()
	}

	fmt.Println("Index Completed")
}
