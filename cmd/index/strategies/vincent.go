package strategies

import (
	"io"
	"no_vcs/me/art-near-me/pkg/domain"
	"os"
)

// VincentIndex everything needed to index vincent van gogh
type VincentIndex struct{}

// GetName gets the name, fulfills the Indexer interface
func (v VincentIndex) GetName() string {
	return "Vincent Van Gogh"
}

// GetSource returns the source, fulfills the Indexer interface
func (v VincentIndex) GetSource() (io.ReadCloser, error) {
	return os.Open("/go/testdata/list-of-vincent-van-gough-works.html")
}

// Strategy returns the channel in which discovered artwork will be put into
func (v VincentIndex) Strategy(i io.ReadCloser) (chan *domain.Painting, chan error) {
	s := WikiTableStrategy{
		TitleCol:    2,
		LocationCol: 4,
		ImageCol:    1,
	}
	return s.Strategy(i)
}
