package strategies

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/wikipedia"
	"time"

	"github.com/PuerkitoBio/goquery"
)

// Index data type for processing each artist
type Index interface {
	GetName() string
	GetSource() (io.ReadCloser, error)
	Strategy(io.ReadCloser) (chan *domain.Painting, chan error)
}

// WikiTableStrategy is a strategy to parse wikipedia pages that have a table
// please see https://en.wikipedia.org/wiki/List_of_works_by_Vincent_van_Gogh
type WikiTableStrategy struct {
	ArtistName  string
	Source      func() (io.ReadCloser, error)
	TitleCol    int
	LocationCol int
	ImageCol    int
	httpCli     *http.Client
}

func (w WikiTableStrategy) GetName() string {
	return w.ArtistName
}

func (w WikiTableStrategy) GetSource() (io.ReadCloser, error) {
	return w.Source()
}

// WikiTableStrategy a strategy for wikipedia tables
func (w WikiTableStrategy) Strategy(i io.ReadCloser) (chan *domain.Painting, chan error) {
	if w.httpCli == nil {
		w.httpCli = &http.Client{
			Timeout: time.Second * 5,
		}
	}
	paintingChan := make(chan *domain.Painting)
	errorChan := make(chan error)

	go func() {
		var err error

		wiki, err := wikipedia.NewPage(i)
		if err != nil {
			errorChan <- err
		}

		tbl := wiki.FindTable(".wikitable.sortable")

		if tbl == nil {
			errorChan <- errors.New("unable to find table")
			return
		}

		for _, row := range tbl.Rows() {

			painting, err := w.parseRow(row)

			if err != nil {
				errorChan <- err
				continue
			}
			paintingChan <- painting
		}
		close(paintingChan)
		close(errorChan)
	}()

	return paintingChan, errorChan
}

type columnGetter interface {
	GetColumn(col int) *goquery.Selection
}

func (w WikiTableStrategy) parseRow(r columnGetter) (*domain.Painting, error) {
	// grab painting info
	title, err := w.processTitle(r.GetColumn(w.TitleCol))
	if err != nil {
		return nil, err
	}
	//year := tbl.GetColumn()(3) // TODO add back in
	locCol := r.GetColumn(w.LocationCol)

	// process src
	image, err := w.processSrc(r.GetColumn(w.ImageCol))
	if err != nil {
		return nil, err
	}

	url, _ := locCol.Find("a").First().Attr("href")

	var f io.ReadCloser

	if url == "" {
		return nil, fmt.Errorf("unable to parse url: %s", url)
	}

	url = fmt.Sprintf("https://en.wikipedia.org%s", url)
	res, err := w.httpCli.Get(url)
	if err != nil {
		return nil, ErrRequestingUrl(url)
	}
	f = res.Body

	locPage, err := wikipedia.NewLocation(f)

	if err != nil {
		return nil, err
	}

	lng, lat, err := locPage.ParseCoords()

	if err != nil {
		return nil, err
	}
	locTitle, err := locPage.ParseTitle()

	if err != nil {
		return nil, err
	}

	if (lng == 0 && lat == 0) || locTitle == "" {
		// log error here
		return nil, fmt.Errorf("unable to parse lat/lng for location url: %s", url)
	}
	painting := domain.Painting{
		Title:      title,
		SmallImage: image.small,
		LargeImage: image.large,
		Location: &domain.Location{
			Name: locTitle,
			Location: domain.Coords{
				Longitude: lng,
				Latitude:  lat,
			},
		},
	}
	return &painting, nil
}

type image struct {
	small string
	large string
}

func (w WikiTableStrategy) processTitle(s *goquery.Selection) (string, error) {
	if s == nil {
		return "", NewErrUnableToGrabValueFromTable("title", nil)
	}
	v := s.Find("i")
	if v != nil {
		return v.Text(), nil
	}
	return s.Text(), nil
}

func (w WikiTableStrategy) processSrc(s *goquery.Selection) (*image, error) {
	if s == nil {
		return nil, NewErrUnableToGrabValueFromTable("src", nil)
	}

	var image image

	image.small, _ = s.Find("img").First().Attr("src")

	src, ok := s.Find("a").First().Attr("href")

	if ok {
		cli := http.Client{
			Timeout: time.Second * 5,
		}
		res, err := cli.Get("https://en.wikipedia.org" + src)
		if err != nil {
			return nil, NewErrUnableToGrabValueFromTable("src", err)
		}

		f, err := wikipedia.NewFile(res.Body)
		res.Body.Close()
		if err != nil {
			return nil, NewErrUnableToGrabValueFromTable("src", err)
		}

		image.large, err = f.GetImage()
		if err != nil {
			return nil, NewErrUnableToGrabValueFromTable("src", err)
		}
	}

	return &image, nil
}
