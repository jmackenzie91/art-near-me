package strategies

import (
	"io"
	"net/http"
	"no_vcs/me/art-near-me/pkg/domain"
	"time"

	"gopkg.in/birkirb/loggers.v1"
)

type CezanneIndex struct {
	Logger loggers.Contextual
}

func (v CezanneIndex) GetName() string {
	return "Paul Cézanne"
}
func (v CezanneIndex) GetSource() (io.ReadCloser, error) {
	cli := http.Client{
		Timeout: time.Second * 4,
	}

	res, err := cli.Get("https://en.wikipedia.org/wiki/List_of_paintings_by_Paul_C%C3%A9zanne")

	if err != nil {
		return nil, err
	}
	return res.Body, nil
}

func (v CezanneIndex) Strategy(i io.ReadCloser) (chan *domain.Painting, chan error) {
	s := WikiTableStrategy{
		TitleCol:    1,
		LocationCol: 4,
		ImageCol:    0,
	}
	return s.Strategy(i)
}
