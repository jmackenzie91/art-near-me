package strategies

import (
	"fmt"
)

type ErrUnableToGrabValueFromTable struct {
	Attr string
	Err  error
}

func NewErrUnableToGrabValueFromTable(a string, e error) ErrUnableToGrabValueFromTable {
	return ErrUnableToGrabValueFromTable{
		Attr: a,
		Err:  e,
	}
}

func (e ErrUnableToGrabValueFromTable) Error() string {
	return fmt.Sprintf("unable to grab attr: %s, err: %s", e.Attr, e.Err)
}

type errUnableToFindTableElement struct {
	artist string
	el     string
}

func ErrUnableToFindTableElement(a, el string) errUnableToFindTableElement {
	return errUnableToFindTableElement{
		artist: a,
		el:     el,
	}
}

func (e errUnableToFindTableElement) Error() string {
	return fmt.Sprintf("Unable to find table '%s' for artist '%s'", e.artist, e.el)
}

type errRequestingUrl struct {
	url string
}

func ErrRequestingUrl(u string) errRequestingUrl {
	return errRequestingUrl{
		url: u,
	}
}

func (e errRequestingUrl) Error() string {
	return fmt.Sprintf("Unable to load url: '%s'", e.url)
}

type errProcessingPainting struct {
	artist   string
	painting string
	err      error
}

func ErrProcessingPainting(a, p string, err error) errProcessingPainting {
	return errProcessingPainting{
		artist:   a,
		painting: p,
		err:      err,
	}
}
