package strategies

import (
	"io"
	"net/http"
	"no_vcs/me/art-near-me/pkg/domain"
	"time"

	"gopkg.in/birkirb/loggers.v1"
)

type GauguinIndex struct {
	Logger loggers.Contextual
}

func (v GauguinIndex) GetName() string {
	return "Paul Gauguin"
}
func (v GauguinIndex) GetSource() (io.ReadCloser, error) {

	cli := http.Client{
		Timeout: time.Second * 4,
	}

	res, err := cli.Get("https://en.wikipedia.org/wiki/List_of_paintings_by_Paul_Gauguin")

	if err != nil {
		return nil, err
	}

	return res.Body, nil
}

func (v GauguinIndex) Strategy(i io.ReadCloser) (chan *domain.Painting, chan error) {
	s := WikiTableStrategy{
		TitleCol:    2,
		LocationCol: 3,
		ImageCol:    4,
	}
	return s.Strategy(i)
}
