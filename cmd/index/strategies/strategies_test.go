package strategies

import (
	"strings"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
)

func Test_ProcessTitle(t *testing.T) {
	testCases := []struct {
		name     string
		input    string
		expected string
	}{
		{
			name:     "see tfd",
			input:    `<table><td><span style="padding: 0; font-size: x-small; font-weight: bold; color: #000000; background-color: #f9f9f9;"><a href="/wiki/Wikipedia:Templates_for_discussion/Log/2019_June_9#Link_language_wrappers" title="Wikipedia:Templates for discussion/Log/2019 June 9">‹See Tfd›</a></span><span class="languageicon">(in English)</span> <i>Self portrait</i></td></table>`,
			expected: "Self portrait",
		},
	}
	for _, tc := range testCases {
		// arrange
		html := strings.NewReader(tc.input)
		doc, err := goquery.NewDocumentFromReader(html)
		if err != nil {
			panic(err)
		}
		// act
		output, _ := WikiTableStrategy{}.processTitle(doc.Find("td"))
		// assert
		assert.Equal(t, tc.expected, output, "output")
	}
}
