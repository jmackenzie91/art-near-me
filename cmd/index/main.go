package main

import (
	"fmt"
	"io"
	"no_vcs/me/art-near-me/cmd/index/strategies"
	"no_vcs/me/art-near-me/internal"
	"no_vcs/me/art-near-me/pkg/crud"
	"no_vcs/me/art-near-me/pkg/repositories"
	"os"

	"github.com/spf13/cobra"
)

func main() {
	c := internal.NewConfiguration()
	r := internal.NewResolver(c)

	log := r.ResolveLogger()

	dal := crud.NewDAL(r.ResolveDB(), log)

	artistService := New(
		ArtistService{
			artistRepo:   repositories.NewArtists(dal),
			paintingRepo: repositories.NewPaintings(dal),
			locationRepo: repositories.NewLocations(dal),
		},
		log,
	)

	cli := r.ResolveHttpClient()

	i := index{
		logger: log,
		todo: []strategies.Index{
			strategies.WikiTableStrategy{
				ArtistName: "Paul Gauguin",
				Source: func() (io.ReadCloser, error) {

					res, err := cli.Get("https://en.wikipedia.org/wiki/List_of_paintings_by_Paul_Gauguin")

					if err != nil {
						return nil, err
					}

					return res.Body, nil
				},
				TitleCol:    2,
				LocationCol: 3,
				ImageCol:    4,
			},
			strategies.VincentIndex{},
			strategies.GauguinIndex{},
			strategies.CezanneIndex{},
			strategies.MunchIndex{},
		},
		artistService: artistService,
	}

	cmd := &cobra.Command{
		Use:   "index",
		Short: "",
		Long:  ``,
		Run:   i.Run,
	}

	if err := cmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
