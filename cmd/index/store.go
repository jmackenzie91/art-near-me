package main

import (
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/repositories"

	"gopkg.in/birkirb/loggers.v1"
)

type ArtistStore struct {
	*domain.Artist
	paintings []domain.Painting
}

func (a ArtistStore) HasPainting(i *domain.Painting) bool {
	for _, p := range a.paintings {
		if p.Title == i.Title {
			return true
		}
	}
	return false
}

// ArtistService wrapper for repositories
type ArtistService struct {
	artistRepo   repositories.IArtists
	paintingRepo repositories.IPaintings
	locationRepo repositories.ILocations
}

// FindArtist returns artist or nil, error on error
func (a ArtistService) FindArtist(n string) (*domain.Artist, error) {
	return a.artistRepo.FindByNameWithPaintings(n)
}

// AddArtist adds the inputted artist
func (a ArtistService) AddArtist(artist *domain.Artist) error {
	return a.artistRepo.AddArtist(artist)
}

// FindLocation finds the inputted location
func (a ArtistService) FindLocation(loc *domain.Location) (*domain.Location, error) {
	return a.locationRepo.Find(loc.Name)
}

// AddLocation adds the location, returns only error
func (a ArtistService) AddLocation(loc *domain.Location) error {
	return a.locationRepo.Add(loc)
}

// AddPainting adds a painting
func (a ArtistService) AddPainting(painting *domain.Painting, artist *domain.Artist) error {
	return a.paintingRepo.Add(painting, artist)
}

// ArtistLoggingService a wrapper for the above service, logs out output
type ArtistLoggingService struct {
	service ArtistService
	logger  loggers.Advanced
	err     error
}

// New constructor for ArtistLoggingService
func New(s ArtistService, l loggers.Contextual) ArtistLoggingService {
	e := l.WithFields("location", "ArtistLoggingService")
	return ArtistLoggingService{
		service: s,
		logger:  e,
	}
}

// GetError returns an error if there has been one,
// patttern to cut down on error checking
func (a *ArtistLoggingService) GetError() error {
	err := a.err
	a.err = nil
	return err
}

// FindArtist returns artist or nil, error on error
func (a ArtistLoggingService) FindArtist(n string) *domain.Artist {
	if a.err != nil {
		return nil
	}
	out, err := a.service.artistRepo.FindByNameWithPaintings(n)
	if err != nil {
		a.logger.Error(err)
		a.err = err
	}
	return out
}

// AddArtist adds the inputted artist
func (a ArtistLoggingService) AddArtist(artist *domain.Artist) {
	if a.err != nil {
		return
	}
	err := a.service.artistRepo.AddArtist(artist)
	if err != nil {
		a.logger.Error(err)
		a.err = err
		return
	}
	a.logger.Infof("new artist added %s", artist.Name)
}

// FindLocation finds the inputted location
func (a ArtistLoggingService) FindLocation(loc *domain.Location) *domain.Location {
	if a.err != nil {
		return nil
	}
	out, err := a.service.locationRepo.Find(loc.Name)
	if err != nil {
		a.logger.Error(err)
		a.err = err
	}
	return out
}

// AddLocation adds the location, returns only error
func (a ArtistLoggingService) AddLocation(loc *domain.Location) {
	if a.err != nil {
		return
	}
	err := a.service.locationRepo.Add(loc)
	if err != nil {
		a.logger.Error(err)
		a.err = err
		return
	}
	a.logger.Infof("new location added %s", loc.Name)
}

// AddPainting adds a painting
func (a ArtistLoggingService) AddPainting(painting *domain.Painting, artist *domain.Artist) {
	if a.err != nil {
		return
	}
	err := a.service.paintingRepo.Add(painting, artist)
	if err != nil {
		a.logger.Error(err)
		a.err = err
		return
	}
	a.logger.Infof("new painting added %s", painting.Title)
}
