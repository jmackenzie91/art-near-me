# Art Near Me

![Prototype Screenshot](https://gitlab.com/jmackenzie91/art-near-me/raw/master/docs/proto-screenshot.png)

> A Geo aware app for discovering famous artwork near you

---

### Table of Contents

- [Description](#description)
- [How To Use](#how-to-use)
- [References](#references)
- [License](#license)
- [Author Info](#author-info)

---

## Description

### What

Art Near Me is still in its development phase and only atm consists of a backend api with four post-impressionist artists;
Van Gogh, Paul Gauguin, Paul Cézanne, and Edvard Munch, and the geo locations of all their artwork. I am looking for a partner/s to help build the frontend and add to its existing functionality.

## Why
To get more people to see the great works of art.

I found myself sat at home and thought "I wonder where the nearest Van Gogh artwork is to me".

## How
The backend web app and indexing scripts are written in go. Why? Because go is scalable, concurrent and cool. 
The frontend is yet to be implemented.

## Next Steps

Are you a frontend dev looking for a part time project? I need help writing the frontend app.
The tech stack is completely up to your choosing.

#### Technologies

- Golang
- Frontend? well that is upto you

[Back To The Top](#art-near-me)

---

## How To Use

#### Installation

Please ensure that you have [Docker CE installed](https://docs.docker.com/install/) along with [Docker Compose](https://docs.docker.com/compose/)
I also make heavy use of makefiles so [make](https://www.gnu.org/software/make/manual/html_node/Introduction.html) would be helpful also.


#### Getting Started

To get started run `make init`

Which basically runs
```cgo
cp example.env .env &&
make build
```

#### API Reference

A list of all the api endpoints and their schemas can be found in this repo at ./docs/schema.html

[Back To The Top](#art-near-me)

---

## References
[Back To The Top](#art-near-me)

---

## License

MIT License

[Back To The Top](#art-near-me)

---

## Author Info

- Twitter - [@JohnnMackk](https://twitter.com/JohnnMackk)
- Website - [John Mackenzie](https://johnmackenzie.co.uk/)

[Back To The Top](#art-near-me)
