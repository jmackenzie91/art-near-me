module no_vcs/me/art-near-me

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/DATA-DOG/godog v0.7.13
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/birkirb/loggers-mapper-logrus v0.0.0-20180326232643-461f2d8e6f72
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/mux v1.7.3
	github.com/johnmackenzie91/go-pagination v0.0.0-20190621081910-1be349f24058
	github.com/magiconair/properties v1.8.0
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.2.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.4
	github.com/stretchr/testify v1.4.0
	github.com/ulule/limiter/v3 v3.2.0
	google.golang.org/appengine v1.6.0 // indirect
	gopkg.in/birkirb/loggers.v1 v1.1.0
)
