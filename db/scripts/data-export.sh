#!/bin/sh

echo "Exporting API database tables"

mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} ${MYSQL_DATABASE} \
    --skip-opt --quick --no-create-info --no-create-db --disable-keys --set-charset --skip-dump-date --skip-comments \
    > /docker-entrypoint-initdb.d/01_init_data.sql
