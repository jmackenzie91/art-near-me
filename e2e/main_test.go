package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"reflect"
	"strconv"
	"testing"
	"time"

	"github.com/DATA-DOG/godog/gherkin"

	"github.com/DATA-DOG/godog"
	"github.com/DATA-DOG/godog/colors"
)

var opt = godog.Options{Output: colors.Colored(os.Stdout)}
var runEndToEndTests = flag.Bool("e2e", false, "Run the integration tests (in addition to the unit tests)")
var CurrentRequest *http.Request
var CurrentResponse *http.Response

func init() {
	godog.BindFlags("godog.", flag.CommandLine, &opt)
}

func TestMain(m *testing.M) {
	flag.Parse()
	opt.Paths = flag.Args()

	// is go test -e2e set?
	if !*runEndToEndTests {
		os.Exit(0)
	}

	status := godog.RunWithOptions("godogs", func(s *godog.Suite) {
		FeatureContext(s)
	}, opt)

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func iBuildARequestTo(method, url string) (err error) {
	CurrentRequest, err = http.NewRequest(method, url, nil)
	return err
}

func iSendToRequest() (err error) {
	client := http.Client{
		Timeout: time.Second * 10,
	}
	CurrentResponse, err = client.Do(CurrentRequest)
	return err
}

func theRequestResponseStatusCodeShouldBe(expected string) error {
	codeToInt, err := strconv.Atoi(expected)

	if err != nil {
		return err
	}
	if CurrentResponse.StatusCode != codeToInt {
		return fmt.Errorf(
			"incorrect status code returned, expected: %d, got: %d",
			codeToInt,
			CurrentResponse.StatusCode,
		)
	}
	return nil
}

func theResponseShouldMatchJSON(body *gherkin.DocString) (err error) {
	var expected, actual interface{}

	// re-encode expected response
	if err = json.Unmarshal([]byte(body.Content), &expected); err != nil {
		return
	}

	// re-encode actual response too
	b, err := ioutil.ReadAll(CurrentResponse.Body)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(b, &actual); err != nil {
		return
	}

	// the matching may be adapted per different requirements.
	if !reflect.DeepEqual(expected, actual) {
		return fmt.Errorf("expected JSON does not match actual, %v vs. %v", expected, actual)
	}
	return nil
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^i build a "([^"]*)" request to "([^"]*)"$`, iBuildARequestTo)
	s.Step(`^I send to request$`, iSendToRequest)
	s.Step(`^the request response status code should be "([^"]*)"$`, theRequestResponseStatusCodeShouldBe)
	s.Step(`^the response should match json:$`, theResponseShouldMatchJSON)
}
