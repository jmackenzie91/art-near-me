Feature: artists

  Scenario: /artists returns artists?limit=2
    Given i build a "GET" request to "http://art-near-me-throttler:8079/artists?limit=2"
    When I send to request
    Then the request response status code should be "200"
	And the response should match json:
	  """
{
    "data": [
        {
            "id": 1,
            "name": "Vincent Van Gogh",
            "image": ""
        },
        {
            "id": 2,
            "name": "Paul Gauguin",
            "image": ""
        }
    ],
    "meta": {
        "total": 4,
        "limit": 2,
        "prev": null,
        "current": "/artists?page=1&limit=2",
        "next": "/artists?page=2&limit=2"
    }
}
	  """

  Scenario: /artists returns artists?page=2&limit=3
    Given i build a "GET" request to "http://art-near-me-throttler:8079/artists?page=2&limit=3"
    When I send to request
    Then the request response status code should be "200"
    And the response should match json:
	  """
{
    "data": [
        {
            "id": 4,
            "name": "Edvard Munch",
            "image": ""
        }
    ],
    "meta": {
        "total": 4,
        "limit": 3,
        "prev": "/artists?page=1&limit=3",
        "current": "/artists?page=2&limit=3",
        "next": null
    }
}
	  """
