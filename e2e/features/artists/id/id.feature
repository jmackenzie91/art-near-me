Feature: artists-id

  Scenario: /artists/{id} returns as expected
    Given i build a "GET" request to "http://art-near-me-throttler:8079/artists/1"
    When I send to request
    Then the request response status code should be "200"
    And the response should match json:
	  """
{
    "data": {
        "id": 1,
        "name": "Vincent Van Gogh",
        "image": ""
    }
}
	  """
