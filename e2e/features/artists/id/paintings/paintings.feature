Feature: artists/id/paintings endpoint

  Scenario: /artists/{id}/paintings returns as expected
    Given i build a "GET" request to "http://art-near-me-throttler/artists/1/paintings?limit=3"
    When I send to request
    Then the request response status code should be "200"
    And the response should match json:
	  """
{
    "data": [
        {
            "id": 1,
            "title": "Crouching Boy with Sickle",
            "small_image": "//upload.wikimedia.org/wikipedia/commons/thumb/2/29/Van_Gogh_-_Kauernder_Junge_mit_Sichel.jpeg/100px-Van_Gogh_-_Kauernder_Junge_mit_Sichel.jpeg",
            "large_image": "//upload.wikimedia.org/wikipedia/commons/2/29/Van_Gogh_-_Kauernder_Junge_mit_Sichel.jpeg",
            "year": "",
            "location": {
                "id": 1,
                "name": "Kröller-Müller Museum",
                "location": {
                    "Longitude": 5.816944122314453,
                    "Latitude": 52.09555435180664
                }
            }
        },
        {
            "id": 2,
            "title": "Still Life with Cabbage and Clogs",
            "small_image": "//upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Still_Life_with_Cabbage_and_Clogs.jpg/100px-Still_Life_with_Cabbage_and_Clogs.jpg",
            "large_image": "//upload.wikimedia.org/wikipedia/commons/1/1e/Still_Life_with_Cabbage_and_Clogs.jpg",
            "year": "",
            "location": {
                "id": 1,
                "name": "Kröller-Müller Museum",
                "location": {
                    "Longitude": 5.816944122314453,
                    "Latitude": 52.09555435180664
                }
            }
        },
        {
            "id": 3,
            "title": "Old Man at the Fireside",
            "small_image": "//upload.wikimedia.org/wikipedia/commons/thumb/c/c8/An_Old_Man_Putting_Dry_Rice_on_the_Hearth_1881_Vincent_van_Gogh.jpg/100px-An_Old_Man_Putting_Dry_Rice_on_the_Hearth_1881_Vincent_van_Gogh.jpg",
            "large_image": "//upload.wikimedia.org/wikipedia/commons/c/c8/An_Old_Man_Putting_Dry_Rice_on_the_Hearth_1881_Vincent_van_Gogh.jpg",
            "year": "",
            "location": {
                "id": 1,
                "name": "Kröller-Müller Museum",
                "location": {
                    "Longitude": 5.816944122314453,
                    "Latitude": 52.09555435180664
                }
            }
        }
    ],
    "meta": {
        "total": 612,
        "limit": 3,
        "prev": null,
        "current": "/artists/1/paintings?page=1&limit=3",
        "next": "/artists/1/paintings?page=2&limit=3"
    }
}
	  """
