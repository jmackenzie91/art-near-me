package internal

import "no_vcs/me/art-near-me/internal/db"

// Config ties together all other application configuration types.
type Config struct {
	DB db.Config
}

// NewConfiguration returns a new configuration struct
func NewConfiguration() *Config {
	return &Config{
		DB: db.GetConfig(),
	}
}
