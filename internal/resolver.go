package internal

import (
	"database/sql"
	"net/http"
	"no_vcs/me/art-near-me/pkg/env"
	"time"

	mapper "github.com/birkirb/loggers-mapper-logrus"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gomodule/redigo/redis"
	"github.com/sirupsen/logrus"
	"gopkg.in/birkirb/loggers.v1"
)

// Resolver resolves all services that app will use
type Resolver struct {
	config *Config

	logger     loggers.Contextual
	db         *sql.DB
	redis      redis.Conn
	httpClient *http.Client
}

// NewResolver returns a new instance of Resolver
func NewResolver(c *Config) *Resolver {
	return &Resolver{
		config: c,
	}
}

// LoggerResolver rmember the interface segregation principle :)
type LoggerResolver interface {
	ResolveLogger() loggers.Contextual
}

// ResolveLogger resolves the logger
func (r *Resolver) ResolveLogger() loggers.Contextual {
	if r.logger == nil {

		l := logrus.New()
		l.SetFormatter(&logrus.JSONFormatter{})

		if env.IsTest() {
			l.SetLevel(logrus.DebugLevel)
		}
		r.logger = mapper.NewLogger(l)
	}
	return r.logger
}

// ResolveDB resolves the db
func (r *Resolver) ResolveDB() *sql.DB {
	if r.db == nil {
		db, err := sql.Open("mysql", r.config.DB.GetConnectionString())
		if err != nil {
			panic(err)
		}

		// find a place for these
		db.SetMaxIdleConns(5)
		db.SetMaxOpenConns(5)

		return db
	}
	return r.db
}

// ResolveRedis resolves redis
func (r *Resolver) ResolveRedis() redis.Conn {
	if r.redis == nil {
		conn, err := redis.Dial("tcp", "art-near-me-redis:6379")
		if err != nil {
			panic(err)
		}
		r.redis = conn
	}
	return r.redis
}

func (r *Resolver) ResolveHttpClient() *http.Client {
	if r.httpClient == nil {
		r.httpClient = &http.Client{
			Timeout: time.Second * 5,
		}
	}
	return r.httpClient
}
