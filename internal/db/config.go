package db

import (
	"fmt"
	"os"
)

// Config holds all the db connection information
type Config struct {
	dbUser string
	dbPass string
	dbHost string
	dbPort string
	dbName string
}

// GetConfig gets the db connection information from env vars,
// is the env vars are not set, app panics
func GetConfig() Config {
	return Config{
		dbUser: getMandidatoryEnvVar("DB_USER"),
		dbPass: getMandidatoryEnvVar("DB_PASS"),
		dbHost: getMandidatoryEnvVar("DB_HOST"),
		dbPort: getMandidatoryEnvVar("DB_PORT"),
		dbName: getMandidatoryEnvVar("DB_NAME"),
	}
}

// GetConnectionString returns a formatted sql connection string
func (c Config) GetConnectionString() string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?parseTime=true", c.dbUser, c.dbPass, c.dbHost, c.dbPort, c.dbName)
}

func getMandidatoryEnvVar(key string) string {
	val := os.Getenv(key)
	if val == "" {
		panic(fmt.Sprintf("mandidatory env var not set %s", key))
	}
	return val
}
