package caches

import (
	"encoding/json"
	"no_vcs/me/art-near-me/pkg/domain"

	"github.com/gomodule/redigo/redis"
	"gopkg.in/birkirb/loggers.v1"
)

// ArtistCache presents two methods, Get, and Set
type ArtistCache interface {
	Get(key string) (*domain.Artist, error)
	Set(key string, artist *domain.Artist) error
}

// NewArtistCache returns a new instance of the artists cache
func NewArtistCache(c redis.Conn, l loggers.Contextual) ArtistCache {
	return artistCache{
		cache:  c,
		logger: l,
	}
}

type artistCache struct {
	cache  redis.Conn
	logger loggers.Contextual
}

// Get returns artist from cache
func (a artistCache) Get(key string) (*domain.Artist, error) {
	a.logger.Infof("GET: %s", key)
	objStr, err := redis.String(a.cache.Do("GET", key))
	if err != nil {
		if err != redis.ErrNil {
			return nil, err
		}
		return nil, nil
	}
	b := []byte(objStr)

	artist := domain.Artist{}

	err = json.Unmarshal(b, &artist)
	if err != nil {
		return nil, err
	}
	return &artist, nil
}

// Set sets the artist to the cache
func (a artistCache) Set(key string, artist *domain.Artist) error {
	a.logger.Infof("SET: %s, Value: %+v\n", key, artist)

	b, err := json.Marshal(artist)
	if err != nil {
		return err
	}
	if _, err := a.cache.Do("SET", key, string(b)); err != nil {
		return err
	}
	return nil
}
