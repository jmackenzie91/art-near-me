package domain

// Painting is peice of art iteself
// TODO rename to artwork? to be more inclusive?
type Painting struct {
	ID         int       `json:"id"`
	Title      string    `json:"title"`
	SmallImage string    `json:"small_image"`
	LargeImage string    `json:"large_image"`
	Year       string    `json:"year"`
	Location   *Location `json:"location"`
}
