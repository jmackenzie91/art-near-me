package domain

import (
	"fmt"
	"regexp"
	"strconv"
)

// Location of the artwork
type Location struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Location Coords `json:"location"`
}

// HasID TODO can we remove this?
func (l Location) HasID() bool {
	return l.ID != 0
}

// Coords is the geo location of the location
type Coords struct {
	Longitude float64
	Latitude  float64
}

type errUnableToParseCoord string

func (e errUnableToParseCoord) Error() string {
	return fmt.Sprintf("unable to parse coord: %s", string(e))
}

// CoordsFromString takes a string and attempts to parse it into a coords struct
func CoordsFromString(s string) (*Coords, error) {

	pattern := `(-?[0-9]{1,3}(?:\.[0-9]{1,10}))`
	match, err := regexp.MatchString(pattern, s)
	if err != nil {
		return nil, err
	}

	if !match {
		return nil, errUnableToParseCoord(0)
	}

	c := Coords{}

	var digitsRegexp = regexp.MustCompile(pattern)
	values := digitsRegexp.FindAllString(s, -1)

	if len(values) != 2 {
		return nil, errUnableToParseCoord(0)
	}

	lng, err := strconv.ParseFloat(values[1], 32)
	if err != nil {
		return nil, errUnableToParseCoord(s)
	}
	c.Longitude = lng
	lat, err := strconv.ParseFloat(values[0], 32)
	if err != nil {
		return nil, errUnableToParseCoord(s)
	}
	c.Latitude = lat
	return &c, nil
}
