package domain_test

import (
	"encoding/json"
	"no_vcs/me/art-near-me/pkg/domain"
	"testing"

	"github.com/magiconair/properties/assert"
)

func Test_MarshalJSON(t *testing.T) {
	testCases := []struct {
		Name           string
		input          domain.Artist
		expectedOutput string
	}{
		{
			Name: "marshals paintings",
			input: domain.Artist{
				ID:    1,
				Name:  "Some Artist",
				Image: "img.jpg",
				Paintings: []domain.Painting{
					{
						ID:         1,
						Title:      "Some painting",
						SmallImage: "small_img.jpg",
						LargeImage: "large_img.jpg",
						Year:       "2012",
						Location: &domain.Location{
							ID:   1,
							Name: "Manchester",
							Location: domain.Coords{
								Longitude: 1.1,
								Latitude:  2.2,
							},
						},
					},
				},
			},
			expectedOutput: `{"id":1,"name":"Some Artist","image":"img.jpg","paintings":[{"id":1,"title":"Some painting","small_image":"small_img.jpg","large_image":"large_img.jpg","year":"2012","location":{"id":1,"name":"Manchester","location":{"Longitude":1.1,"Latitude":2.2}}}]}`,
		},
		{
			Name: "marshals without painting",
			input: domain.Artist{
				ID:        1,
				Name:      "Some Artist",
				Image:     "img.jpg",
				Paintings: nil,
			},
			expectedOutput: `{"id":1,"name":"Some Artist","image":"img.jpg"}`,
		},
	}
	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			// arrange
			// act
			output, err := json.Marshal(tc.input)
			if err != nil {
				panic(err)
			}
			// assert
			assert.Equal(t, string(output), tc.expectedOutput)
		})
	}
}
