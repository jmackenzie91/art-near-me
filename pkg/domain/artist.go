package domain

import "encoding/json"

// ArtistCollection is a collection of artists
type ArtistCollection struct {
	Collection []Artist
}

// Artists is the painter themselves
type Artist struct {
	ID        int        `json:"id"`
	Name      string     `json:"name"`
	Image     string     `json:"image"`
	Paintings []Painting `json:"paintings"`
}

// HasID TODO remove this?
func (a Artist) HasID() bool {
	return a.ID != 0
}

func (a Artist) MarshalJSON() ([]byte, error) {
	if a.Paintings == nil || len(a.Paintings) == 0 {
		return json.Marshal(struct {
			ID    int    `json:"id"`
			Name  string `json:"name"`
			Image string `json:"image"`
		}{
			ID:    a.ID,
			Name:  a.Name,
			Image: a.Image,
		})
	}
	return json.Marshal(struct {
		ID        int        `json:"id"`
		Name      string     `json:"name"`
		Image     string     `json:"image"`
		Paintings []Painting `json:"paintings"`
	}{
		ID:        a.ID,
		Name:      a.Name,
		Image:     a.Image,
		Paintings: a.Paintings,
	})
}
