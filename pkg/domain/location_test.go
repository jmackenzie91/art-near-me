package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCoordsFromString(t *testing.T) {
	testCases := []struct {
		input          string
		expectedOutput *Coords
		expectedErr    error
	}{
		{
			input: "53.79648,-1.54785",
			expectedOutput: &Coords{
				Latitude:  53.796478271484375,
				Longitude: -1.5478500127792358,
			},
		},
		{
			input:       "525432",
			expectedErr: errUnableToParseCoord(0),
		},
		{
			input:       "52.52000813.404954",
			expectedErr: errUnableToParseCoord(0),
		},
	}
	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			// arrange
			// act
			output, outputErr := CoordsFromString(tc.input)
			// assert
			assert.Equal(t, tc.expectedOutput, output, "output")
			assert.Equal(t, tc.expectedErr, outputErr, "error")
		})
	}
}
