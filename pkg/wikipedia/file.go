package wikipedia

import (
	"errors"
	"io"

	"github.com/PuerkitoBio/goquery"
)

type file struct {
	doc *goquery.Document
}

// NewFile returns a wikipedia file type
func NewFile(i io.ReadCloser) (*file, error) {
	var err error
	f := file{}
	f.doc, err = goquery.NewDocumentFromReader(i)
	if err != nil {
		return nil, err
	}
	return &f, nil
}

// GetImage parses the file page and returns the the image, supposing that is is an image
func (f file) GetImage() (string, error) {
	img, ok := f.doc.Find("#file a").Attr("href")
	if !ok {
		return "", errors.New("No image found")
	}
	return img, nil
}
