package wikipedia

import (
	"io"

	"github.com/PuerkitoBio/goquery"
)

type wikipedia struct {
	doc *goquery.Document
}

// NewPage returns a generic page
func NewPage(content io.ReadCloser) (*wikipedia, error) {
	var err error
	w := wikipedia{}
	w.doc, err = goquery.NewDocumentFromReader(content)
	if err != nil {
		return nil, err
	}
	return &w, nil
}

// FindTable find the table on page for inputted selector
func (w wikipedia) FindTable(selector string) *table {
	tableSelec := w.doc.Find(selector)

	if tableSelec == nil {
		return nil
	}

	return &table{
		selection: tableSelec,
	}
}

// NewLocation returns a new wikipedia location page
func NewLocation(content io.ReadCloser) (*location, error) {

	doc, err := goquery.NewDocumentFromReader(content)
	if err != nil {
		return nil, err
	}

	return &location{
		doc: doc,
	}, nil
}
