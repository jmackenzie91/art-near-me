package wikipedia

import (
	"errors"
	"no_vcs/me/art-near-me/pkg/latlng"
	"regexp"

	"github.com/PuerkitoBio/goquery"
)

var rx *regexp.Regexp

func init() {
	var err error
	if rx, err = regexp.Compile(`[-?0-9\.]+`); err != nil {
		panic(err)
	}
}

type location struct {
	doc *goquery.Document
}

// ParseTitle parses the page for the title and returns it
func (l location) ParseTitle() (string, error) {
	title := l.doc.Find("#firstHeading").First().Text()
	if title == "" {
		return "", errors.New("No title found")
	}
	return title, nil
}

// ParseCoords parses the page for the coordinates
func (l location) ParseCoords() (float64, float64, error) {

	str := l.doc.Find(".geo-dms").First().Text()

	latlng, err := latlng.FromString(str)

	if err != nil {
		return 0, 0, err
	}

	return latlng.Longitude, latlng.Latitude, nil
}
