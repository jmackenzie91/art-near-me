package wikipedia

import (
	"os"
	"testing"

	"github.com/PuerkitoBio/goquery"
	"github.com/stretchr/testify/assert"
)

func TestLocation_ParseCoords(t *testing.T) {
	testCases := []struct {
		name        string
		inputSource string
		expectedLng float64
		expectedLat float64
		expectedErr error
	}{
		{
			name:        "extracts coords as expected",
			inputSource: "../../testdata/location.html",
			expectedLng: 5.816944,
			expectedLat: 52.095556,
			expectedErr: nil,
		},
	}

	for _, tc := range testCases {
		// Arrange
		file, err := os.Open(tc.inputSource)
		if err != nil {
			panic(err)
		}
		doc, err := goquery.NewDocumentFromReader(file)
		if err != nil {
			panic(err)
		}

		loc := location{
			doc: doc,
		}

		// Act
		lng, lat, err := loc.ParseCoords()

		// Assert
		assert.Equal(t, tc.expectedLng, lng, tc.name)
		assert.Equal(t, tc.expectedLat, lat, tc.name)
		assert.Equal(t, tc.expectedErr, err, tc.name)
	}
}
