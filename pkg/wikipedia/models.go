package wikipedia

import "github.com/PuerkitoBio/goquery"

type table struct {
	selection *goquery.Selection
}

// Rows return all rows for table, can be interated over
func (t table) Rows() []tableRow {
	var selec []tableRow
	t.selection.Find("tr").Each(func(k int, s *goquery.Selection) {
		selec = append(selec, tableRow{
			selection: s,
		})
	})
	return selec
}

type tableRow struct {
	selection *goquery.Selection
}

// GetColumn returns a specific column from within the row
func (tr tableRow) GetColumn(col int) *goquery.Selection {
	var selct *goquery.Selection
	tr.selection.Children().Each(func(i int, s *goquery.Selection) {

		if i == col {
			selct = s
		}
	})
	return selct
}
