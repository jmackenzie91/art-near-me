// Code generated by moq; DO NOT EDIT.
// github.com/matryer/moq

package repositories

import (
	"no_vcs/me/art-near-me/pkg/domain"
	"sync"
)

var (
	lockIArtistsMockAddArtist               sync.RWMutex
	lockIArtistsMockCount                   sync.RWMutex
	lockIArtistsMockFindAll                 sync.RWMutex
	lockIArtistsMockFindByID                sync.RWMutex
	lockIArtistsMockFindByIDWithPaintings   sync.RWMutex
	lockIArtistsMockFindByName              sync.RWMutex
	lockIArtistsMockFindByNameWithPaintings sync.RWMutex
)

// Ensure, that IArtistsMock does implement IArtists.
// If this is not the case, regenerate this file with moq.
var _ IArtists = &IArtistsMock{}

// IArtistsMock is a mock implementation of IArtists.
//
//     func TestSomethingThatUsesIArtists(t *testing.T) {
//
//         // make and configure a mocked IArtists
//         mockedIArtists := &IArtistsMock{
//             AddArtistFunc: func(in1 *domain.Artist) error {
// 	               panic("mock out the AddArtist method")
//             },
//             CountFunc: func() (int, error) {
// 	               panic("mock out the Count method")
//             },
//             FindAllFunc: func(in1 int, in2 int) ([]domain.Artist, error) {
// 	               panic("mock out the FindAll method")
//             },
//             FindByIDFunc: func(id int) (*domain.Artist, error) {
// 	               panic("mock out the FindByID method")
//             },
//             FindByIDWithPaintingsFunc: func(in1 int, in2 int, in3 int) (*domain.Artist, error) {
// 	               panic("mock out the FindByIDWithPaintings method")
//             },
//             FindByNameFunc: func(in1 string) (*domain.Artist, error) {
// 	               panic("mock out the FindByName method")
//             },
//             FindByNameWithPaintingsFunc: func(n string) (*domain.Artist, error) {
// 	               panic("mock out the FindByNameWithPaintings method")
//             },
//         }
//
//         // use mockedIArtists in code that requires IArtists
//         // and then make assertions.
//
//     }
type IArtistsMock struct {
	// AddArtistFunc mocks the AddArtist method.
	AddArtistFunc func(in1 *domain.Artist) error

	// CountFunc mocks the Count method.
	CountFunc func() (int, error)

	// FindAllFunc mocks the FindAll method.
	FindAllFunc func(in1 int, in2 int) ([]domain.Artist, error)

	// FindByIDFunc mocks the FindByID method.
	FindByIDFunc func(id int) (*domain.Artist, error)

	// FindByIDWithPaintingsFunc mocks the FindByIDWithPaintings method.
	FindByIDWithPaintingsFunc func(in1 int, in2 int, in3 int) (*domain.Artist, error)

	// FindByNameFunc mocks the FindByName method.
	FindByNameFunc func(in1 string) (*domain.Artist, error)

	// FindByNameWithPaintingsFunc mocks the FindByNameWithPaintings method.
	FindByNameWithPaintingsFunc func(n string) (*domain.Artist, error)

	// calls tracks calls to the methods.
	calls struct {
		// AddArtist holds details about calls to the AddArtist method.
		AddArtist []struct {
			// In1 is the in1 argument value.
			In1 *domain.Artist
		}
		// Count holds details about calls to the Count method.
		Count []struct {
		}
		// FindAll holds details about calls to the FindAll method.
		FindAll []struct {
			// In1 is the in1 argument value.
			In1 int
			// In2 is the in2 argument value.
			In2 int
		}
		// FindByID holds details about calls to the FindByID method.
		FindByID []struct {
			// ID is the id argument value.
			ID int
		}
		// FindByIDWithPaintings holds details about calls to the FindByIDWithPaintings method.
		FindByIDWithPaintings []struct {
			// In1 is the in1 argument value.
			In1 int
			// In2 is the in2 argument value.
			In2 int
			// In3 is the in3 argument value.
			In3 int
		}
		// FindByName holds details about calls to the FindByName method.
		FindByName []struct {
			// In1 is the in1 argument value.
			In1 string
		}
		// FindByNameWithPaintings holds details about calls to the FindByNameWithPaintings method.
		FindByNameWithPaintings []struct {
			// N is the n argument value.
			N string
		}
	}
}

// AddArtist calls AddArtistFunc.
func (mock *IArtistsMock) AddArtist(in1 *domain.Artist) error {
	if mock.AddArtistFunc == nil {
		panic("IArtistsMock.AddArtistFunc: method is nil but IArtists.AddArtist was just called")
	}
	callInfo := struct {
		In1 *domain.Artist
	}{
		In1: in1,
	}
	lockIArtistsMockAddArtist.Lock()
	mock.calls.AddArtist = append(mock.calls.AddArtist, callInfo)
	lockIArtistsMockAddArtist.Unlock()
	return mock.AddArtistFunc(in1)
}

// AddArtistCalls gets all the calls that were made to AddArtist.
// Check the length with:
//     len(mockedIArtists.AddArtistCalls())
func (mock *IArtistsMock) AddArtistCalls() []struct {
	In1 *domain.Artist
} {
	var calls []struct {
		In1 *domain.Artist
	}
	lockIArtistsMockAddArtist.RLock()
	calls = mock.calls.AddArtist
	lockIArtistsMockAddArtist.RUnlock()
	return calls
}

// Count calls CountFunc.
func (mock *IArtistsMock) Count() (int, error) {
	if mock.CountFunc == nil {
		panic("IArtistsMock.CountFunc: method is nil but IArtists.Count was just called")
	}
	callInfo := struct {
	}{}
	lockIArtistsMockCount.Lock()
	mock.calls.Count = append(mock.calls.Count, callInfo)
	lockIArtistsMockCount.Unlock()
	return mock.CountFunc()
}

// CountCalls gets all the calls that were made to Count.
// Check the length with:
//     len(mockedIArtists.CountCalls())
func (mock *IArtistsMock) CountCalls() []struct {
} {
	var calls []struct {
	}
	lockIArtistsMockCount.RLock()
	calls = mock.calls.Count
	lockIArtistsMockCount.RUnlock()
	return calls
}

// FindAll calls FindAllFunc.
func (mock *IArtistsMock) FindAll(in1 int, in2 int) ([]domain.Artist, error) {
	if mock.FindAllFunc == nil {
		panic("IArtistsMock.FindAllFunc: method is nil but IArtists.FindAll was just called")
	}
	callInfo := struct {
		In1 int
		In2 int
	}{
		In1: in1,
		In2: in2,
	}
	lockIArtistsMockFindAll.Lock()
	mock.calls.FindAll = append(mock.calls.FindAll, callInfo)
	lockIArtistsMockFindAll.Unlock()
	return mock.FindAllFunc(in1, in2)
}

// FindAllCalls gets all the calls that were made to FindAll.
// Check the length with:
//     len(mockedIArtists.FindAllCalls())
func (mock *IArtistsMock) FindAllCalls() []struct {
	In1 int
	In2 int
} {
	var calls []struct {
		In1 int
		In2 int
	}
	lockIArtistsMockFindAll.RLock()
	calls = mock.calls.FindAll
	lockIArtistsMockFindAll.RUnlock()
	return calls
}

// FindByID calls FindByIDFunc.
func (mock *IArtistsMock) FindByID(id int) (*domain.Artist, error) {
	if mock.FindByIDFunc == nil {
		panic("IArtistsMock.FindByIDFunc: method is nil but IArtists.FindByID was just called")
	}
	callInfo := struct {
		ID int
	}{
		ID: id,
	}
	lockIArtistsMockFindByID.Lock()
	mock.calls.FindByID = append(mock.calls.FindByID, callInfo)
	lockIArtistsMockFindByID.Unlock()
	return mock.FindByIDFunc(id)
}

// FindByIDCalls gets all the calls that were made to FindByID.
// Check the length with:
//     len(mockedIArtists.FindByIDCalls())
func (mock *IArtistsMock) FindByIDCalls() []struct {
	ID int
} {
	var calls []struct {
		ID int
	}
	lockIArtistsMockFindByID.RLock()
	calls = mock.calls.FindByID
	lockIArtistsMockFindByID.RUnlock()
	return calls
}

// FindByIDWithPaintings calls FindByIDWithPaintingsFunc.
func (mock *IArtistsMock) FindByIDWithPaintings(in1 int, in2 int, in3 int) (*domain.Artist, error) {
	if mock.FindByIDWithPaintingsFunc == nil {
		panic("IArtistsMock.FindByIDWithPaintingsFunc: method is nil but IArtists.FindByIDWithPaintings was just called")
	}
	callInfo := struct {
		In1 int
		In2 int
		In3 int
	}{
		In1: in1,
		In2: in2,
		In3: in3,
	}
	lockIArtistsMockFindByIDWithPaintings.Lock()
	mock.calls.FindByIDWithPaintings = append(mock.calls.FindByIDWithPaintings, callInfo)
	lockIArtistsMockFindByIDWithPaintings.Unlock()
	return mock.FindByIDWithPaintingsFunc(in1, in2, in3)
}

// FindByIDWithPaintingsCalls gets all the calls that were made to FindByIDWithPaintings.
// Check the length with:
//     len(mockedIArtists.FindByIDWithPaintingsCalls())
func (mock *IArtistsMock) FindByIDWithPaintingsCalls() []struct {
	In1 int
	In2 int
	In3 int
} {
	var calls []struct {
		In1 int
		In2 int
		In3 int
	}
	lockIArtistsMockFindByIDWithPaintings.RLock()
	calls = mock.calls.FindByIDWithPaintings
	lockIArtistsMockFindByIDWithPaintings.RUnlock()
	return calls
}

// FindByName calls FindByNameFunc.
func (mock *IArtistsMock) FindByName(in1 string) (*domain.Artist, error) {
	if mock.FindByNameFunc == nil {
		panic("IArtistsMock.FindByNameFunc: method is nil but IArtists.FindByName was just called")
	}
	callInfo := struct {
		In1 string
	}{
		In1: in1,
	}
	lockIArtistsMockFindByName.Lock()
	mock.calls.FindByName = append(mock.calls.FindByName, callInfo)
	lockIArtistsMockFindByName.Unlock()
	return mock.FindByNameFunc(in1)
}

// FindByNameCalls gets all the calls that were made to FindByName.
// Check the length with:
//     len(mockedIArtists.FindByNameCalls())
func (mock *IArtistsMock) FindByNameCalls() []struct {
	In1 string
} {
	var calls []struct {
		In1 string
	}
	lockIArtistsMockFindByName.RLock()
	calls = mock.calls.FindByName
	lockIArtistsMockFindByName.RUnlock()
	return calls
}

// FindByNameWithPaintings calls FindByNameWithPaintingsFunc.
func (mock *IArtistsMock) FindByNameWithPaintings(n string) (*domain.Artist, error) {
	if mock.FindByNameWithPaintingsFunc == nil {
		panic("IArtistsMock.FindByNameWithPaintingsFunc: method is nil but IArtists.FindByNameWithPaintings was just called")
	}
	callInfo := struct {
		N string
	}{
		N: n,
	}
	lockIArtistsMockFindByNameWithPaintings.Lock()
	mock.calls.FindByNameWithPaintings = append(mock.calls.FindByNameWithPaintings, callInfo)
	lockIArtistsMockFindByNameWithPaintings.Unlock()
	return mock.FindByNameWithPaintingsFunc(n)
}

// FindByNameWithPaintingsCalls gets all the calls that were made to FindByNameWithPaintings.
// Check the length with:
//     len(mockedIArtists.FindByNameWithPaintingsCalls())
func (mock *IArtistsMock) FindByNameWithPaintingsCalls() []struct {
	N string
} {
	var calls []struct {
		N string
	}
	lockIArtistsMockFindByNameWithPaintings.RLock()
	calls = mock.calls.FindByNameWithPaintings
	lockIArtistsMockFindByNameWithPaintings.RUnlock()
	return calls
}
