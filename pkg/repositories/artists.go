package repositories

import (
	"no_vcs/me/art-near-me/pkg/crud"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/sequel/models"
	"no_vcs/me/art-near-me/pkg/sequel/queries"
)

// IArtists interface for all repo functions
type IArtists interface {
	Count() (int, error)
	FindAll(int, int) ([]domain.Artist, error)
	FindByIDWithPaintings(int, int, int) (*domain.Artist, error)
	FindByNameWithPaintings(n string) (*domain.Artist, error)
	FindByName(string) (*domain.Artist, error)
	AddArtist(*domain.Artist) error
	FindByID(id int) (*domain.Artist, error)
}

type artists struct {
	dal crud.DAL
}

// NewArtists returns an struct which contains the IArtists functionality
func NewArtists(dal crud.DAL) IArtists {
	return artists{
		dal: dal,
	}
}

// FindByName finds the artist by name
func (a artists) FindByName(n string) (*domain.Artist, error) {
	q := queries.FindArtistsByName(n)
	m := domain.Artist{}
	mw := models.SqlArtist{
		Artist: &m,
	}
	a.dal.SetStrategy(mw.BindSingle)
	err := a.dal.Query(q)
	return &m, err
}

// FindByID finds artist by id
func (a artists) FindByID(id int) (*domain.Artist, error) {
	q := queries.FindArtistsById(id)
	m := domain.Artist{}
	mw := models.SqlArtist{
		Artist: &m,
	}
	a.dal.SetStrategy(mw.BindSingle)
	err := a.dal.Query(q)
	return &m, err
}

// FindByNameWithPaintings same sa above but returns paintings also
func (a artists) FindByNameWithPaintings(n string) (*domain.Artist, error) {
	q := queries.FindArtistsByNameWithPaintings(n)
	m := domain.Artist{}
	mw := models.SqlArtist{
		Artist: &m,
	}
	a.dal.SetStrategy(mw.BindSingleWithPaintings)
	err := a.dal.Query(q)

	if err != nil {
		return nil, err
	}
	if m.HasID() {
		return &m, nil
	}

	return nil, err
}

// FindByIDWithPaintings same as above but queries by id instead of name
func (a artists) FindByIDWithPaintings(id, o, l int) (*domain.Artist, error) {
	q := queries.FindArtistsByIdWithPaintings{
		Id:     id,
		Offset: o,
		Limit:  l,
	}
	m := domain.Artist{}
	mw := models.SqlArtist{
		Artist: &m,
	}
	a.dal.SetStrategy(mw.BindSingleWithPaintings)
	err := a.dal.Query(q)
	return &m, err
}

// AddArtist adds artist to db
func (a artists) AddArtist(artist *domain.Artist) error {
	q := queries.InsertArtist(*artist)
	mw := models.SqlArtist{
		Artist: artist,
	}
	a.dal.SetResult(mw.Result)
	return a.dal.Exec(q)
}

// FindAll find all artists
func (a artists) FindAll(offset, limit int) ([]domain.Artist, error) {
	q := queries.SelectAllArtists{
		Limit:  limit,
		Offset: offset,
	}
	artists := domain.ArtistCollection{}
	artistsW := models.SQLArtistCollection{
		ArtistCollection: &artists,
	}
	a.dal.SetStrategy(artistsW.BindAll)
	err := a.dal.Query(q)
	return artistsW.Collection, err
}

// Count the number of artits is data source
func (a artists) Count() (int, error) {
	q := queries.CountAllArtists{}
	count := models.Count{}
	a.dal.SetStrategy(count.BindCount)
	err := a.dal.Query(q)

	if err != nil {
		return 0, err
	}
	return count.Value, err
}
