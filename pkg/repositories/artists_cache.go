package repositories

import (
	"fmt"
	"no_vcs/me/art-near-me/pkg/caches"
	"no_vcs/me/art-near-me/pkg/domain"

	"gopkg.in/birkirb/loggers.v1"
)

type artistsCache struct {
	repo   IArtists
	cache  caches.ArtistCache
	logger loggers.Contextual
}

// NewArtistsCache returns a new instance of IArtists, that caches itself
func NewArtistsCache(r IArtists, l loggers.Contextual, c caches.ArtistCache) IArtists {
	return artistsCache{
		repo:   r,
		cache:  c,
		logger: l,
	}
}

// FindByName finds an artist by name
func (a artistsCache) FindByName(n string) (*domain.Artist, error) {
	return a.repo.FindByName(n)
}

// FindByID returns artist that matches ID, this version of function caches
func (a artistsCache) FindByID(ID int) (artist *domain.Artist, err error) {
	key := fmt.Sprintf("%d_FindByIDWithPaintings", ID)

	// try and get from cache, if there is an error || the artist exists, return
	if artist, err = a.cache.Get(key); err != nil || artist != nil {
		return artist, err
	}

	if artist, err = a.repo.FindByID(ID); err != nil {
		return nil, err
	}

	err = a.cache.Set(key, artist)
	return artist, err
}

// FindByNameWithPaintings returns with artist with their paintings which matches inputted name
func (a artistsCache) FindByNameWithPaintings(n string) (*domain.Artist, error) {
	return a.repo.FindByNameWithPaintings(n)
}

// FindByIDWithPaintings returns with artist with their paintings which matches inputted ID
func (a artistsCache) FindByIDWithPaintings(ID, o, l int) (artist *domain.Artist, err error) {
	key := fmt.Sprintf("%d_%d_%d_FindByIDWithPaintings", ID, o, l)

	// try and get from cache, if there is an error || the artist exists, return
	if artist, err := a.cache.Get(key); err != nil || artist != nil {
		return artist, err
	}

	if artist, err = a.repo.FindByIDWithPaintings(ID, o, l); err != nil {
		return nil, err
	}

	err = a.cache.Set(key, artist)
	return artist, err
}

// AddArtist inserts an artist into the db
func (a artistsCache) AddArtist(artist *domain.Artist) error {
	return a.repo.AddArtist(artist)
}

// FindAll returns all artists, takes offset & limit
func (a artistsCache) FindAll(offset, limit int) ([]domain.Artist, error) {
	return a.repo.FindAll(offset, limit)
}

// Count counts all artists in db
func (a artistsCache) Count() (int, error) {
	return a.repo.Count()
}
