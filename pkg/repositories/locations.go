package repositories

import (
	"no_vcs/me/art-near-me/pkg/crud"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/sequel/models"
	"no_vcs/me/art-near-me/pkg/sequel/queries"
)

type ILocations interface {
	Add(*domain.Location) error
	Find(n string) (*domain.Location, error)
}

type locations struct {
	dal crud.DAL
}

func NewLocations(dal crud.DAL) ILocations {
	return locations{
		dal: dal,
	}
}

func (p locations) Add(l *domain.Location) error {
	q := queries.InsertLocation(*l)
	lw := models.SQLLocation{
		Location: l,
	}
	p.dal.SetResult(lw.Result)
	return p.dal.Exec(q)
}

func (p locations) Find(n string) (*domain.Location, error) {
	q := queries.SelectLocation(n)
	m := domain.Location{}
	mw := models.SQLLocation{
		Location: &m,
	}
	p.dal.SetStrategy(mw.BindSingle)
	err := p.dal.Query(q)

	if err != nil {
		return nil, err
	}

	if m.HasID() {
		return &m, nil
	}
	return nil, nil
}
