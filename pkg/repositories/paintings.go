package repositories

import (
	"no_vcs/me/art-near-me/pkg/crud"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/pkg/sequel/models"
	"no_vcs/me/art-near-me/pkg/sequel/queries"
)

// IPaintings interface for all repo functions
type IPaintings interface {
	Add(*domain.Painting, *domain.Artist) error
	Count(a *domain.Artist) (int, error)
	FindByLocation(location *domain.Coords, distance int, limit int) ([]domain.Painting, error)
}

type paintings struct {
	dal crud.DAL
}

// NewPaintings returns an struct which contains the IPaintings functionality
func NewPaintings(dal crud.DAL) IPaintings {
	return paintings{
		dal: dal,
	}
}

// Add adds a new painting to the data source
func (p paintings) Add(painting *domain.Painting, artist *domain.Artist) error {
	q := queries.InsertPainting{
		Painting: *painting,
		Artist:   *artist,
	}
	pw := models.SQLPainting{
		Painting: painting,
	}
	p.dal.SetResult(pw.Result)
	return p.dal.Exec(q)
}

// Count returns the total number of paintings by artist
func (p paintings) Count(a *domain.Artist) (int, error) {
	q := queries.CountPaintingsByArtist(*a)
	count := models.Count{}
	p.dal.SetStrategy(count.BindCount)
	err := p.dal.Query(q)

	if err != nil {
		return 0, err
	}
	return count.Value, err
}

// FindByLocation takes lat/lng and returns all paintings from within a certain distance
func (p paintings) FindByLocation(location *domain.Coords, d, l int) ([]domain.Painting, error) {
	q := queries.SelectPaintingsByDistance{
		Location: location,
		Distance: d,
		Limit:    l,
	}
	col := models.PaintingsCollection{}
	p.dal.SetStrategy(col.BindMany)
	err := p.dal.Query(q)
	if err != nil {
		return nil, err
	}
	return col.Data, nil
}
