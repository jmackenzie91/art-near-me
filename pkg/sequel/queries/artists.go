package queries

import "no_vcs/me/art-near-me/pkg/domain"

// FindArtistsByName finds artist by name
type FindArtistsByName string

// GetCommand returns the sql statement to run
func (f FindArtistsByName) GetCommand() string {
	return "SELECT id, name, image FROM artists WHERE name = ? LIMIT 1"
}

// GetParams returns params needed by query
func (f FindArtistsByName) GetParams() []interface{} {
	return []interface{}{f}
}

// FindArtistsById finds artist by id
type FindArtistsById int

// GetCommand returns the sql statement to run
func (f FindArtistsById) GetCommand() string {
	return "SELECT id, name, image FROM artists WHERE id = ? LIMIT 1"
}

// GetParams returns params needed by query
func (f FindArtistsById) GetParams() []interface{} {
	return []interface{}{f}
}

// SelectAllArtists selects all of the artists
type SelectAllArtists struct {
	Limit  int
	Offset int
}

// GetCommand returns the sql statement to run
func (f SelectAllArtists) GetCommand() string {
	return "SELECT id, name, image FROM artists LIMIT ? OFFSET ?"
}

// GetParams returns params needed by query
func (f SelectAllArtists) GetParams() []interface{} {
	limit := f.Limit
	if limit == 0 {
		limit = 10
	}
	return []interface{}{limit, f.Offset}
}

// FindArtistsByNameWithPaintings finds artist by name
type FindArtistsByNameWithPaintings string

// GetCommand returns the sql statement to run
func (f FindArtistsByNameWithPaintings) GetCommand() string {
	return `SELECT a.id, a.name, a.image, p.id, p.title, p.small_image, p.large_image, l.id, l.name, AsText(l.location)
FROM artists a
JOIN paintings p ON p.artist_id = a.id
JOIN locations l ON l.id = p.location_id
WHERE a.name = ?`
}

// GetParams returns params needed by query
func (f FindArtistsByNameWithPaintings) GetParams() []interface{} {
	return []interface{}{f}
}

// FindArtistsByIdWithPaintings finds artist by id and also returns their paintings
type FindArtistsByIdWithPaintings struct {
	Id     int
	Offset int
	Limit  int
}

// GetCommand returns the sql statement to run
func (f FindArtistsByIdWithPaintings) GetCommand() string {
	return `SELECT a.id, a.name, a.image, p.id, p.title, p.small_image, p.large_image, l.id, l.name, AsText(l.location)
FROM artists a
JOIN paintings p ON p.artist_id = a.id
JOIN locations l ON l.id = p.location_id
WHERE a.id = ? LIMIT ? OFFSET ?`
}

// GetParams returns params needed by query
func (f FindArtistsByIdWithPaintings) GetParams() []interface{} {
	return []interface{}{f.Id, f.Limit, f.Offset}
}

//InsertArtist inserts an artist into the db
type InsertArtist domain.Artist

// GetCommand returns the sql statement to run
func (i InsertArtist) GetCommand() string {
	return `INSERT INTO artists (name, image) VALUES(?,?)`
}

// GetParams returns params needed by query
func (i InsertArtist) GetParams() []interface{} {
	return []interface{}{i.Name, i.Image}
}

// CountAllArtists count all artists in db
type CountAllArtists struct{}

// GetCommand returns the sql statement to run
func (f CountAllArtists) GetCommand() string {
	return "SELECT COUNT(*) FROM artists"
}

// GetParams returns params needed by query
func (f CountAllArtists) GetParams() []interface{} {
	return []interface{}{}
}
