package queries

import "no_vcs/me/art-near-me/pkg/domain"

// InsertLocation inserts a location into db
type InsertLocation domain.Location

// GetCommand returns the sql statement to run
func (i InsertLocation) GetCommand() string {
	return "INSERT INTO locations (name, location) VALUES(?,POINT(?,?))"
}

// GetParams returns params needed by query
func (i InsertLocation) GetParams() []interface{} {
	return []interface{}{i.Name, i.Location.Longitude, i.Location.Latitude}
}

// SelectLocation selects locations from db
type SelectLocation string

// GetCommand returns the sql statement to run
func (i SelectLocation) GetCommand() string {
	return "SELECT id, AsText(location) FROM locations WHERE name = ? LIMIT 1"
}

// GetParams returns params needed by query
func (i SelectLocation) GetParams() []interface{} {
	return []interface{}{i}
}
