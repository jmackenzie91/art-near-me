package queries

import "no_vcs/me/art-near-me/pkg/domain"

// InsertPainting iserts a painting into the db
type InsertPainting struct {
	Painting domain.Painting
	Artist   domain.Artist
}

// GetCommand returns the sql
func (f InsertPainting) GetCommand() string {
	return "INSERT INTO paintings (title, small_image, large_image, artist_id, location_id) VALUES(?,?,?,?,?)"
}

// GetParams returns the params needed for query
func (f InsertPainting) GetParams() []interface{} {
	return []interface{}{f.Painting.Title, f.Painting.SmallImage, f.Painting.LargeImage, f.Artist.ID, f.Painting.Location.ID}
}

// CountPaintingsByArtist returns count of all artists in db
type CountPaintingsByArtist domain.Artist

// GetCommand returns the sql
func (c CountPaintingsByArtist) GetCommand() string {
	return "SELECT COUNT(*) FROM paintings WHERE artist_id = ?"
}

// GetParams returns the params needed for query
func (c CountPaintingsByArtist) GetParams() []interface{} {
	return []interface{}{c.ID}
}

// SelectPaintingsByDistance returns paintings that are from within a certain distance
type SelectPaintingsByDistance struct {
	Location *domain.Coords
	Distance int
	Limit    int
}

// GetCommand returns the sql
func (f SelectPaintingsByDistance) GetCommand() string {
	return `
SELECT
	p.id, p.title, p.small_image, p.large_image, l.id, l.name, AsText(l.location)
FROM
	locations l
JOIN paintings p ON l.id = p.location_id
WHERE
	ST_Distance_Sphere(l.location, POINT(?,?)) < ?
ORDER BY ST_Distance_Sphere(l.location, POINT(?,?)) ASC LIMIT ?`
}

// GetParams returns the params needed for query
func (c SelectPaintingsByDistance) GetParams() []interface{} {
	return []interface{}{
		c.Location.Longitude,
		c.Location.Latitude,
		c.Distance,
		c.Location.Longitude,
		c.Location.Latitude,
		c.Limit,
	}
}
