package models

import (
	"database/sql"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/testdata"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func TestSQLPainting_Result(t *testing.T) {
	testCases := []struct {
		name           string
		inputRows      *sql.Rows
		expectedOutput []domain.Painting
		expectedError  error
	}{
		{
			name: "bind as expected",
			inputRows: testdata.MockRowsToSqlRows(
				sqlmock.NewRows([]string{"p.id", "p.title", "p.small_image", "p.large_image", "l.id", "l.name", "l.location"}).
					AddRow(1, "painting one", "small_image", "large_image", 1, "some museum", "POINT(1.1 2.2)").
					AddRow(2, "painting two", "small_image", "large_image", 1, "some museum", "POINT(1.1 2.2)").
					AddRow(3, "painting three", "small_image", "large_image", 2, "another museum", "POINT(3.3 4.4)"),
			),
			expectedOutput: []domain.Painting{
				{
					ID:         1,
					Title:      "painting one",
					SmallImage: "small_image",
					LargeImage: "large_image",
					Location: &domain.Location{
						ID:   1,
						Name: "some museum",
						Location: domain.Coords{
							Longitude: 1.100000023841858,
							Latitude:  2.200000047683716,
						},
					},
				},
				{
					ID:         2,
					Title:      "painting two",
					SmallImage: "small_image",
					LargeImage: "large_image",
					Location: &domain.Location{
						ID:   1,
						Name: "some museum",
						Location: domain.Coords{
							Longitude: 1.100000023841858,
							Latitude:  2.200000047683716,
						},
					},
				},
				{
					ID:         3,
					Title:      "painting three",
					SmallImage: "small_image",
					LargeImage: "large_image",
					Location: &domain.Location{
						ID:   2,
						Name: "another museum",
						Location: domain.Coords{
							Longitude: 3.299999952316284,
							Latitude:  4.400000095367432,
						},
					},
				},
			},
		},
	}
	for _, tc := range testCases {
		// arrange
		m := PaintingsCollection{}
		// act
		err := m.BindMany(tc.inputRows)
		// assert
		assert.Equal(t, tc.expectedOutput, m.Data, "artist")
		assert.Equal(t, tc.expectedError, err, "error")
	}
}
