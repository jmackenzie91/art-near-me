package models

import (
	"database/sql"
	"no_vcs/me/art-near-me/pkg/domain"
)

// SQLPainting an adapter for the domain.Painting struct, adds extra funcs
type SQLPainting struct {
	*domain.Painting
}

// Result parses and returns an sql result
func (p *SQLPainting) Result(res sql.Result) error {
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	p.ID = int(id)
	return nil
}

// PaintingsCollection type for multiple rows of sql
type PaintingsCollection struct {
	Data []domain.Painting
}

// BindMany binds multiple sql results
func (p *PaintingsCollection) BindMany(r *sql.Rows) error {
	for r.Next() {
		painting := domain.Painting{
			Location: &domain.Location{},
		}

		currentCoords := SqlCoords{
			&domain.Coords{},
		}

		err := r.Scan(
			&painting.ID,
			&painting.Title,
			&painting.SmallImage,
			&painting.LargeImage,
			&painting.Location.ID,
			&painting.Location.Name,
			&currentCoords,
		)

		if err != nil {
			return err
		}

		painting.Location.Location = *currentCoords.Coords

		p.Data = append(p.Data, painting)
	}
	return nil
}
