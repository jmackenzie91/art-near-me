package models

import (
	"database/sql"
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/testdata"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

func Test_BindSingleWithPaintings(t *testing.T) {

	testCases := []struct {
		name           string
		inputRows      *sql.Rows
		expectedOutput *domain.Artist
		expectedError  error
	}{
		{
			name: "bind as expected",
			inputRows: testdata.MockRowsToSqlRows(
				sqlmock.NewRows([]string{"a.id", "a.name", "a.image", "p.id", "p.title", "p.small_image", "p.large_image", "l.id", "p.name", "p.location"}).
					AddRow(1, "artist one", "artist_image", 2, "painting title", "painting_image", "painting_image_2", 3, "some museum", "POINT(1.1 2.2)").
					AddRow(1, "artist one", "artist_image", 3, "second picture", "painting_image_2", "painting_image_2_2", 3, "some museum", "POINT(1.1 2.2)")),
			expectedOutput: &domain.Artist{
				ID:    1,
				Name:  "artist one",
				Image: "artist_image",
				Paintings: []domain.Painting{
					{
						ID:         2,
						Title:      "painting title",
						SmallImage: "painting_image",
						LargeImage: "painting_image_2",
						Location: &domain.Location{
							ID:   3,
							Name: "some museum",
							Location: domain.Coords{
								Longitude: 1.100000023841858,
								Latitude:  2.200000047683716,
							},
						},
					},
					{
						ID:         3,
						Title:      "second picture",
						SmallImage: "painting_image_2",
						LargeImage: "painting_image_2_2",
						Location: &domain.Location{
							ID:   3,
							Name: "some museum",
							Location: domain.Coords{
								Longitude: 1.100000023841858,
								Latitude:  2.200000047683716,
							},
						},
					},
				},
			},
		},
	}
	for _, tc := range testCases {
		// arrange
		m := domain.Artist{}
		mw := SqlArtist{
			&m,
		}
		// act
		err := mw.BindSingleWithPaintings(tc.inputRows)
		// assert
		assert.Equal(t, tc.expectedOutput, mw.Artist, "artist")
		assert.Equal(t, tc.expectedError, err, "error")
	}
}
