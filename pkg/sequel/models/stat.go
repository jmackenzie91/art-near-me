package models

import (
	"database/sql"
)

// Count used for parsing sql count(*) query
type Count struct {
	Value int
}

// BindCount binds the result of count(*)
func (c *Count) BindCount(rows *sql.Rows) error {
	if rows.Next() {
		err := rows.Scan(&c.Value)
		if err != nil {
			return err
		}
	}
	return nil
}
