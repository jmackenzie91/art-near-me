package models

import (
	"database/sql/driver"
	"fmt"
	"no_vcs/me/art-near-me/pkg/domain"
	"reflect"
	"regexp"
	"strconv"
)

// SqlCoords an adapter for the domain.Coords struct, adds extra funcs
type SqlCoords struct {
	*domain.Coords
}

// ErrNotAPointer returned when the value passed in is not a pointer
type ErrNotAPointer string

// Error implements the error interface
func (e ErrNotAPointer) Error() string {
	return fmt.Sprintf("unable to scan as pointer: %s", string(e))
}

// ErrIncorrectType returns when value inputted into scan is not either a []byte or string
type ErrIncorrectType string

// Error implements the error interface
func (e ErrIncorrectType) Error() string {
	return fmt.Sprintf("incorrect type returned from db: %s must be string", string(e))
}

// Scan implements the Scanner interface
func (c *SqlCoords) Scan(v interface{}) error {
	vAsByte, byteOk := v.([]byte)
	vAsStr, strOk := v.(string)
	if !byteOk && !strOk {
		return ErrIncorrectType(reflect.TypeOf(v).String())
	}

	var s string
	if byteOk {
		s = string(vAsByte)
	} else {
		s = vAsStr
	}

	pattern := `(-?[0-9]{1,3}(?:\.[0-9]{1,10}))|([0-9])`
	match, _ := regexp.MatchString(pattern, s)
	if !match {
		return ErrNotAPointer(s)
	}

	var digitsRegexp = regexp.MustCompile(pattern)
	values := digitsRegexp.FindAllString(s, -1)

	lng, err := strconv.ParseFloat(values[0], 32)
	if err != nil {
		return ErrNotAPointer(s)
	}
	c.Longitude = lng
	lat, err := strconv.ParseFloat(values[1], 32)
	if err != nil {
		return ErrNotAPointer(s)
	}
	c.Latitude = lat
	return nil
}

// Value implements the Valuer interface
func (c *SqlCoords) Value() (driver.Value, error) {
	return fmt.Sprintf("POINT(%f,%f)", c.Longitude, c.Latitude), nil
}
