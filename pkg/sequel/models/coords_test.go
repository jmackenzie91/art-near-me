package models

import (
	"no_vcs/me/art-near-me/pkg/domain"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSqlCoords_Scan(t *testing.T) {
	testCases := []struct {
		input          string
		expectedOutput *domain.Coords
		expectedErr    error
	}{
		{
			input: "POINT(1.1 2.2)",
			expectedOutput: &domain.Coords{
				Longitude: 1.100000023841858,
				Latitude:  2.200000047683716,
			},
		},
		{
			input: "POINT(51.667 5)",
			expectedOutput: &domain.Coords{
				Longitude: 51.66699981689453,
				Latitude:  5,
			},
		},
		{
			input: "POINT(42.339167 -71.094167)",
			expectedOutput: &domain.Coords{
				Longitude: 42.339168548583984,
				Latitude:  -71.09416961669922,
			},
		},
		{
			input: "POINT(38.63944 -90.29443999999999)",
			expectedOutput: &domain.Coords{
				Longitude: 38.63943862915039,
				Latitude:  -90.29444122314453,
			},
		},
		{
			input: "POINT(39.966 -75.181)",
			expectedOutput: &domain.Coords{
				Longitude: 39.965999603271484,
				Latitude:  -75.18099975585938,
			},
		},
		{
			input: "POINT(41.308459 -72.93098500000001))",
			expectedOutput: &domain.Coords{
				Longitude: 41.3084602355957,
				Latitude:  -72.93098449707031,
			},
		},
		{
			input: "POINT(40.782975 -73.95899199999999))",
			expectedOutput: &domain.Coords{
				Longitude: 40.78297424316406,
				Latitude:  -73.95899200439453,
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.input, func(t *testing.T) {
			// arrange
			c := domain.Coords{}
			sut := SqlCoords{
				&c,
			}
			// act
			err := sut.Scan(tc.input)
			// assert
			assert.Equal(t, tc.expectedOutput, sut.Coords, "output")
			assert.Equal(t, tc.expectedErr, err, "error")
		})
	}
}
