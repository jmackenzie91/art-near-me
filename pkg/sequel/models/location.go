package models

import (
	"database/sql"
	"no_vcs/me/art-near-me/pkg/domain"
)

// SQLLocation an adapter for the domain.Location struct, adds extra funcs
type SQLLocation struct {
	*domain.Location
}

// BindSingle binds sql result for location only. &a.ID, &coords
func (a *SQLLocation) BindSingle(rows *sql.Rows) error {
	if rows.Next() {

		if rows.Err() != nil {
			return rows.Err()
		}

		coords := SqlCoords{
			&domain.Coords{},
		}

		err := rows.Scan(&a.ID, &coords)
		if err != nil {
			return err
		}
		a.Location.Location = *coords.Coords
	}
	return nil
}

// Result parses and returns an sql result
func (l *SQLLocation) Result(res sql.Result) error {
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	l.ID = int(id)
	return nil
}

// SQLArtistCollection type for multiple rows of sql
type SQLArtistCollection struct {
	*domain.ArtistCollection
}

// Bind rows of artists, with no other joins
func (a *SQLArtistCollection) Bind(rows *sql.Rows) error {
	for rows.Next() {
		rowArtist := domain.Artist{}

		if err := rows.Scan(&rowArtist.ID, &rowArtist.Name, &rowArtist.Image); err != nil {
			return nil
		}
		a.Collection = append(a.Collection, rowArtist)
	}
	return nil
}

// BindAll TODO is this the same as above?
func (a *SQLArtistCollection) BindAll(rows *sql.Rows) error {
	for rows.Next() {
		rowArtist := domain.Artist{}

		if err := rows.Scan(&rowArtist.ID, &rowArtist.Name, &rowArtist.Image); err != nil {
			return err
		}
		a.Collection = append(a.Collection, rowArtist)
	}
	return nil
}
