package models

import (
	"database/sql"
	"no_vcs/me/art-near-me/pkg/domain"
)

// SqlArtist an adapter for the domain.Painting struct, adds extra funcs
type SqlArtist struct {
	*domain.Artist
}

// BindSingle binds sql result for artist only. &a.ID, &a.Name, &a.Image
func (a *SqlArtist) BindSingle(rows *sql.Rows) error {
	if rows.Next() {

		if rows.Err() != nil {
			return rows.Err()
		}

		err := rows.Scan(&a.ID, &a.Name, &a.Image)

		if err != nil {
			return err
		}
	}
	return nil
}

// Result parses and returns an sql result
func (a *SqlArtist) Result(res sql.Result) error {
	id, err := res.LastInsertId()
	if err != nil {
		return err
	}
	a.ID = int(id)
	return nil
}

// BindSingleWithPaintings binds a single artist with multiple paintings
func (a *SqlArtist) BindSingleWithPaintings(rows *sql.Rows) error {
	if rows.Next() {
		currentCoords := SqlCoords{
			&domain.Coords{},
		}
		currentLocation := domain.Location{}
		currentPainting := domain.Painting{}

		err := rows.Scan(&a.ID, &a.Name, &a.Image, &currentPainting.ID, &currentPainting.Title, &currentPainting.SmallImage, &currentPainting.LargeImage, &currentLocation.ID, &currentLocation.Name, &currentCoords)

		if err != nil {
			return err
		}
		currentLocation.Location = *currentCoords.Coords
		currentPainting.Location = &currentLocation

		a.Paintings = append(a.Paintings, currentPainting)

		for rows.Next() {

			var blank string
			err := rows.Scan(&blank, &blank, &blank, &currentPainting.ID, &currentPainting.Title, &currentPainting.SmallImage, &currentPainting.LargeImage, &currentLocation.ID, &currentLocation.Name, &currentCoords)
			if err != nil {
				return err
			}

			currentLocation.Location = *currentCoords.Coords
			currentPainting.Location = &currentLocation

			a.Paintings = append(a.Paintings, currentPainting)
		}
	}
	return nil
}
