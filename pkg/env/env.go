package env

import "os"

// IsTest returns whether the current env is test or not
func IsTest() bool {
	return os.Getenv("ENV") == "TEST"
}

// GetEnv returns the current env as string
func GetEnv() string {
	if os.Getenv("ENV") == "TEST" {
		return "TEST"
	}
	return "PROD"
}
