package latlng

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
)

// DMS converts degrees minutes and seconds to decimal lat/lng
func DMS(d1, m1, s1 float64, comp1 string, d2, m2, s2 float64, comp2 string) (decimal, error) {

	lng := d2 + m2/60 + s2/3600

	if comp2 == "W" {
		lng = lng - (lng * 2)
	}

	lat := d1 + m1/60 + s1/3600

	if comp1 == "S" {
		lat = lat - (lat * 2)
	}

	return decimal{
		Latitude:  toFixed(lat, 6),
		Longitude: toFixed(lng, 6),
	}, nil
}

// FromString parses DMS into decmial lat/lng
func FromString(s string) (decimal, error) {
	re := regexp.MustCompile(`[0-9\.$]+|[NESW]`)
	parsed := re.FindAllString(s, -1)
	//str := strings.Split(clean, " ")

	if len(parsed) != 8 {
		return decimal{}, fmt.Errorf("incorrect format %s", parsed)
	}

	d1, _ := strconv.ParseFloat(parsed[0], 64)
	m1, _ := strconv.ParseFloat(parsed[1], 64)
	s1, _ := strconv.ParseFloat(parsed[2], 64)
	comp1 := parsed[3]
	d2, _ := strconv.ParseFloat(parsed[4], 64)
	m2, _ := strconv.ParseFloat(parsed[5], 64)
	s2, _ := strconv.ParseFloat(parsed[6], 64)
	comp2 := parsed[7]

	return DMS(d1, m1, s1, comp1, d2, m2, s2, comp2)

}

type decimal struct {
	Latitude  float64
	Longitude float64
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(math.Round(num*output)) / output
}
