package latlng

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_DMS(t *testing.T) {

	testCases := []struct {
		name           string
		inputDMS       TestDMS
		expectedOutput decimal
	}{
		{
			name: "London",
			inputDMS: TestDMS{
				LatDegree: 51,
				LatMinute: 30,
				LatSecond: 26.4636,
				LatDir:    "N",
				LngDegree: 0,
				LngMinute: 7,
				LngSecond: 39.9288,
				LngDir:    "W",
			},
			expectedOutput: decimal{
				Latitude:  51.507351,
				Longitude: -0.127758,
			},
		},
		{
			name: "Stockholm",
			inputDMS: TestDMS{
				LatDegree: 59,
				LatMinute: 19,
				LatSecond: 45.5628,
				LatDir:    "N",
				LngDegree: 18,
				LngMinute: 4,
				LngSecond: 6.8916,
				LngDir:    "E",
			},
			//"59° 19' 45.5628'' N, 18° 4' 6.8916'' E",
			expectedOutput: decimal{
				Latitude:  59.329323,
				Longitude: 18.068581,
			},
		},
		{
			name: "Buenos Aires",
			inputDMS: TestDMS{
				LatDegree: 34,
				LatMinute: 36,
				LatSecond: 12,
				LatDir:    "S",
				LngDegree: 58,
				LngMinute: 22,
				LngSecond: 54,
				LngDir:    "W",
			},
			expectedOutput: decimal{
				Latitude:  -34.603333,
				Longitude: -58.381667,
			},
		},
		{
			name: "Tymbuktu",
			inputDMS: TestDMS{
				LatDegree: 20,
				LatMinute: 0,
				LatSecond: 40.968,
				LatDir:    "N",
				LngDegree: 3,
				LngMinute: 14,
				LngSecond: 41.604,
				LngDir:    "W",
			},
			expectedOutput: decimal{
				Latitude:  20.011380,
				Longitude: -3.244890,
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// arrange

			// act
			output, _ := DMS(tc.inputDMS.LatDegree, tc.inputDMS.LatMinute, tc.inputDMS.LatSecond, tc.inputDMS.LatDir, tc.inputDMS.LngDegree, tc.inputDMS.LngMinute, tc.inputDMS.LngSecond, tc.inputDMS.LngDir)
			// assert
			assert.Equal(t, tc.expectedOutput, output, "output")
		})
	}
}

func TestFromString(t *testing.T) {

	testCases := []struct {
		name           string
		input          string
		expectedOutput decimal
	}{
		{
			name:  "London",
			input: "51° 30' 26.4636' N, 0° 7' 39.9288' W",
			expectedOutput: decimal{
				Latitude:  51.507351,
				Longitude: -0.127758,
			},
		},
		{
			name:  "van gogh from wikipedia",
			input: "52° 21′ 30″ N, 4° 52′ 52″ E",
			expectedOutput: decimal{
				Latitude:  52.358333,
				Longitude: 4.881111,
			},
		},
	}
	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// arrange
			// act
			output, outputErr := FromString(tc.input)
			// assert
			assert.Equal(t, tc.expectedOutput, output, "output")
			assert.Equal(t, nil, outputErr, "output")
		})
	}
}

type TestDMS struct {
	LatDegree float64
	LatMinute float64
	LatSecond float64
	LatDir    string
	LngDegree float64
	LngMinute float64
	LngSecond float64
	LngDir    string
}
