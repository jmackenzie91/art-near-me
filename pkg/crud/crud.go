package crud

import (
	"database/sql"

	"gopkg.in/birkirb/loggers.v1"
)

type strategy func(*sql.Rows) error
type res func(result sql.Result) error

// DAL the the data access layer, it takes a sql db and a strategy in which to parse the result
type DAL struct {
	db       *sql.DB
	logger   loggers.Contextual
	strategy strategy
	result   res //TODO rename me
}

// NewDAL is a constructor for the DAL
func NewDAL(db *sql.DB, logger loggers.Contextual) DAL {
	return DAL{
		db:     db,
		logger: logger,
	}
}

// SetStrategy setter for the strategy
func (d *DAL) SetStrategy(f strategy) {
	d.strategy = f
}

// SetResult setter for the result strategy
func (d *DAL) SetResult(f res) {
	d.result = f
}

// Query interface will represent the query. This package doe snot care what the type of command (exec or query)
// the command is, or how many params it requirs. Only that it implements the interface
type Query interface {
	GetCommand() string
	GetParams() []interface{}
}

// Exec runs the exec command type
func (d DAL) Exec(q Query) error {
	d.logger.WithField("type", "query").Debug(q.GetCommand())
	res, err := d.db.Exec(q.GetCommand(), q.GetParams()...)
	if err != nil {
		return err
	}
	return d.result(res)
}

// Query runs query command type
func (d DAL) Query(q Query) error {
	d.logger.WithField("type", "query").Debug(q.GetCommand())
	rows, err := d.db.Query(q.GetCommand(), q.GetParams()...)
	if err != nil {
		return err
	}
	defer func() {
		err := rows.Close()
		if err != nil {
			d.logger.Error(err)
		}
	}()
	return d.strategy(rows)
}
