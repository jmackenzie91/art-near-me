package responses

import (
	"no_vcs/me/art-near-me/pkg/domain"
	"no_vcs/me/art-near-me/testdata"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArtistIndex_MarshalJSON(t *testing.T) {
	testCases := []struct {
		name           string
		input          ArtistIndex
		expectedOutput []byte
		expectedError  error
	}{
		{
			name: "marshals as expected",
			input: ArtistIndex{
				Data: []domain.Artist{
					{
						ID:        123,
						Name:      "some artist",
						Image:     "some_image.jpg",
						Paintings: []domain.Painting{}, // this should not get output
					},
					{
						ID:    123,
						Name:  "artist two",
						Image: "artist_two.jpg",
						Paintings: []domain.Painting{
							{
								ID:    123,
								Title: "some painting / should not be displayed",
							},
						}, // this should not get output
					},
				},
			},
			expectedOutput: testdata.GrabFile("../../testdata/artists_index_two-results.json"),
		},
		{
			name: "no results returned",
			input: ArtistIndex{
				Data: nil,
			},
			expectedOutput: []byte("{\"meta\":{\"total\":0},\"data\":[]}"),
		},
	}

	for _, tc := range testCases {
		// arrange
		// act
		output, outputErr := tc.input.MarshalJSON()
		// assert
		assert.Equal(t, string(tc.expectedOutput), string(output), "output")
		assert.Equal(t, tc.expectedError, outputErr, "error")
	}
}
