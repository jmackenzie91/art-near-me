package responses

import (
	"encoding/json"
	"no_vcs/me/art-near-me/pkg/domain"
)

// ArtistIndex represents the json to marshal for the GET /artists endpoint
type ArtistIndex struct {
	Data []domain.Artist `json:"data"`
}

type meta struct {
	Total int `json:"total"`
}
type simpleArtist struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Image string `json:"image"`
}

// MarshalJSON overwrites the default json marshalling for ArtistIndex
func (a ArtistIndex) MarshalJSON() ([]byte, error) {

	out := []simpleArtist{}

	for _, artist := range a.Data {
		out = append(out, simpleArtist{
			ID:    artist.ID,
			Name:  artist.Name,
			Image: artist.Image,
		})
	}

	return json.Marshal(struct {
		Meta meta           `json:"meta"`
		Data []simpleArtist `json:"data"`
	}{
		Meta: meta{
			Total: len(out),
		},
		Data: out,
	})
}

// ArtistId returns a single simopleArtist with no paintings
type ArtistId struct {
	Data *domain.Artist `json:"data"`
}

// MarshalJSON overwrites default MarshalJSON method
func (a ArtistId) MarshalJSON() ([]byte, error) {
	sArtist := simpleArtist{
		ID:    a.Data.ID,
		Name:  a.Data.Name,
		Image: a.Data.Image,
	}
	return json.Marshal(struct {
		Data simpleArtist `json:"data"`
	}{
		Data: sArtist,
	})
}
