package testdata

import (
	"database/sql"
	"io/ioutil"
	"regexp"
	"strings"

	"github.com/DATA-DOG/go-sqlmock"
)

// EscapeQueryString turns a query use in prod, into a regular expression used in test
// query declared once, making tests less brittal
func EscapeQueryString(q string) string {
	q = strings.Replace(q, "?", "\\?", -1)
	q = strings.Replace(q, "(", "\\(", -1)
	return strings.Replace(q, ")", "\\)", -1)
}

// ParseColumns parses all the columns that are present in a sql select query
// this helps setting up sql mocks
func ParseColumns(q string) []string {
	first := strings.Index(q, "SELECT") + 6

	if first == 5 {
		panic("select query not inputted")
	}

	last := strings.Index(q, "FROM")

	r, _ := regexp.Compile("([a-zA-Z_]+)")
	cols := []string{}

	for _, col := range r.FindAllStringSubmatch(q[first:last], -1) {
		cols = append(cols, col[1])
	}
	return cols
}

// CheckErr saves if err != nil ... yawn
func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// GrabFile panics if not available, saves os.Open() ... err
func GrabFile(loc string) []byte {
	s, err := ioutil.ReadFile(loc)
	CheckErr(err)

	return []byte(strings.Replace(string(s), "\n", "", -1))
}

// MockRowsToSqlRows transforms mock sql rows into actual sql rows
func MockRowsToSqlRows(mockRows *sqlmock.Rows) *sql.Rows {
	db, mock, _ := sqlmock.New()
	mock.ExpectQuery("select").WillReturnRows(mockRows)
	rows, _ := db.Query("select")
	return rows
}
