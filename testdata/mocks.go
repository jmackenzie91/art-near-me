package testdata

//
//// IArtistsMock is a mock of the IArtists interface
//type IArtistsMock struct {
//	FindByNameModel *domain.Artist
//	FindByNameError error
//	FindAllModel    []*domain.Artist
//	FindAllError    error
//}
//
//// FindByName returns whatever is set in FindByNameModel and FindByNameError struct above
//func (a IArtistsMock) FindByName(n string) (*domain.Artist, error) {
//	return a.FindByNameModel, a.FindByNameError
//}
//
//// FindByName returns whatever is set in FindAllModel and FindAllError struct above
//func (a IArtistsMock) FindAll() ([]*domain.Artist, error) {
//	return a.FindAllModel, a.FindAllError
//}
//
//// StubLogger is a one liner helper method to implment a new logger
//func StubLogger() loggers.Contextual {
//	log := logrus.New()
//	return mapper.NewLogger(log)
//
//}
