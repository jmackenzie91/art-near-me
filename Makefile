container=art-near-me-app
dbcontainer=art-near-me-db

up:
	docker-compose up -d art-near-me-app

build:
	docker-compose rm -vsf
	docker-compose down -v --remove-orphans
	docker-compose build --parallel
	$(MAKE) up

down:
	docker-compose down

run:
	docker-compose run ${container} -d

jumpin:
	docker-compose run ${container} bash

test:
	go test `go list ./... | grep -v e2e`

test-e2e:
	docker-compose run art-near-me-e2e

tail-logs:
	docker-compose logs -f

tail-errors:
	tail -f art_near_me.log | jq '.'

go-imports-on-file:
	goimports -w $(FILE)

go-lint-on-file:
	golangci-lint run $(FILE) --enable-all

go-lint-on-all:
	golangci-lint run ./ --enable-all

index:
	docker-compose run art-near-me-index /go/bin/index

build-raml:
	raml2html -i=./docs/raml/schema.raml -o=./docs/schema.html

data-export:
	docker-compose exec ${dbcontainer} /db/data-export.sh
