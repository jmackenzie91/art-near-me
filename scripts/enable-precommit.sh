#!/bin/bash

###############################
# Install pre-commit
###############################

if ! [ -x "$(command -v pre-commit)" ]; then
    echo "Installing pre-commit on system.."
    python scripts/pre-commit/installer.py
else
    echo "pre-commit already installed on system"
fi

###############################
# Install / force update
###############################

pre-commit uninstall && pre-commit uninstall -t commit-msg && pre-commit clean && pre-commit install -f --install-hooks && pre-commit install --hook-type commit-msg
